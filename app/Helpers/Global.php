<?php

// use Illuminate\Support\Facades\Auth;
use App\Category_Post;
use App\Post;
use App\Category;

function amountCategory($idCategory) {
    $amountCategory = Category_Post::where('category_id', $idCategory)->count();
    return $amountCategory;
}

function listPost($id) {
    $post = Post::where('id', $id)->with('users')->first();
    return $post;
}

function categories() {
    $categories = Category::select('category_name')->get();
    return $categories;
}

