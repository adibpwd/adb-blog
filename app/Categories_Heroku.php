<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories_Heroku extends Model
{
    protected $table = 'categories_heroku';
    protected $guarded = ['created_at', 'updated_at'];
}
