<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['content', 'author_id', 'post_id'];

    function users() {
        return $this->belongsTo(User::class, 'author_id');
    }

    function posts() {
        return $this->belongsTo(Post::class, 'post_id');
    }
}
