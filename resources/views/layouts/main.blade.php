<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    {{-- @yield('style') --}}

    {{-- <link href="{{ assets('img/favicon.png') }}" rel="icon"> --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/bd2a7f64b3.js" crossorigin="anonymous"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @stack('css')
    <title>@yield('title')</title>
    @yield('head')
</head>

<body>

@yield('content')

    {{-- navbar --}}

    <!-- <div style="position: fixed; z-index: 110; width: 100%; top: 0;">
        <nav class="navbar navbar-expand-lg" style="height: 62px!important; background-color: #34A4E8;">
            <a class="navbar-brand navbar-logo" href="/">xnxx.com</a>
            <button class="navbar-toggler text-white" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <i class="fas fa-bars text-white"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent" style="background-color: #34A4E8;">
                <ul class="navbar-nav ml-auto mr-4">
                    <div class="hori-selector">
                        <div class="left"></div>
                        <div class="right"></div>
                    </div>
                    <li class="nav-item active home">
                        <a class="nav-link" href="javascript:void(0);"><i class="fas fa-home"></i>Home</a>
                    </li>
                    <li class="nav-item btn-category">
                        <a class="nav-link" href="javascript:void(0);"><i class="fas fa-list-ul"></i>Category</a>
                    </li>
                    <li class="nav-item about">
                        <a class="nav-link" href="#hjk"><i class="far fa-address-card"></i>About</a>
                    </li>
    
                </ul>
            </div>
        </nav>
    </div>
    
    @yield('content')

    <br><br><br><br><br><br><br><br>

    {{-- menu navbar --}}

    <div class="mr-2 float-right text-center category" style="opacity: 0.5; color: white; display: none;  width: 25%;">
        <div class="row mt-1 mb-2 mr-1 bg-dark rounded">
            <div class="col-12 col-md-4">hello</div>
            <div class="col-12 col-md-4">hello</div>
            <div class="col-12 col-md-4">hello</div>
            <div class="col-12 col-md-4">hello</div>
            <div class="col-12 col-md-4">hello</div>
            <div class="col-12 col-md-4">hello</div>
            <div class="col-12 col-md-4">hello</div>
            <div class="col-12 col-md-4">hello</div>
        </div>
    </div>



    {{-- footer --}}

    {{-- <div class="main"></div> --}}
    <div class="footer" id="hjk">
      <div class="bubbles" style="width: 92%;">
        <div class="bubble" style="--size:5.305335379905843rem; --distance:8.911402119594282rem; --position:29.507234660856895%; --time:2.0090961262498466s; --delay:-2.9746084640322477s;"></div>
        <div class="bubble" style="--size:3.6390333574974374rem; --distance:6.606964205410751rem; --position:30.24438168126175%; --time:2.6203489859866s; --delay:-3.57114430188496s;"></div>
        <div class="bubble" style="--size:2.220898405413486rem; --distance:9.490888164048757rem; --position:39.35197334222759%; --time:3.770278061545637s; --delay:-2.3549231670849107s;"></div>
        <div class="bubble" style="--size:3.594143230354838rem; --distance:9.520973999518247rem; --position:25.883536665298315%; --time:3.187860785532336s; --delay:-2.130253031356086s;"></div>
        <div class="bubble" style="--size:5.532280167036088rem; --distance:6.598708549379964rem; --position:47.02092782810508%; --time:2.8476582055449886s; --delay:-3.4230968092781002s;"></div>
        <div class="bubble" style="--size:4.181362780182297rem; --distance:7.653749146297846rem; --position:42.38656045949666%; --time:3.394675661910512s; --delay:-3.9396431786404156s;"></div>
        <div class="bubble" style="--size:5.558726625520909rem; --distance:6.698657138643015rem; --position:21.924101582123644%; --time:2.4261372430102703s; --delay:-2.878684632814629s;"></div>
        <div class="bubble" style="--size:2.5901281297528014rem; --distance:8.780914254997729rem; --position:31.761789706483505%; --time:3.1622096157780746s; --delay:-3.182323536003555s;"></div>
        <div class="bubble" style="--size:3.989090198362912rem; --distance:6.75804464630056rem; --position:96.88300847565107%; --time:2.7609731526762498s; --delay:-3.7860623382964147s;"></div>
        <div class="bubble" style="--size:3.551825476195308rem; --distance:6.905160781249367rem; --position:7.444528518566454%; --time:3.497070312133931s; --delay:-2.513632031940331s;"></div>
        <div class="bubble" style="--size:3.462840862092846rem; --distance:7.208659323829014rem; --position:76.88039241934452%; --time:2.9126957720408653s; --delay:-3.7001376336105416s;"></div>
        <div class="bubble" style="--size:4.500960374736724rem; --distance:6.46327466558313rem; --position:91.37372616971916%; --time:2.127993089661933s; --delay:-3.4868331686423333s;"></div>
        <div class="bubble" style="--size:2.900269790878535rem; --distance:9.813522554686948rem; --position:103.71670971668354%; --time:3.811990722071583s; --delay:-3.2625445681305436s;"></div>
        <div class="bubble" style="--size:3.777201279603384rem; --distance:6.4215110742717485rem; --position:62.370811268200484%; --time:3.638092122611964s; --delay:-2.929646309093381s;"></div>
        <div class="bubble" style="--size:5.291332464598028rem; --distance:7.019198368080176rem; --position:94.30789709832514%; --time:2.631023160073992s; --delay:-2.981443831955906s;"></div>
        <div class="bubble" style="--size:3.40130732387286rem; --distance:6.404191609345703rem; --position:47.727975279842475%; --time:2.280436099090325s; --delay:-2.9542646738354685s;"></div>
        <div class="bubble" style="--size:3.8672805694051924rem; --distance:8.087235704425375rem; --position:75.69135090603086%; --time:3.4522880387802606s; --delay:-2.7870839597372936s;"></div>
        <div class="bubble" style="--size:5.66042290464637rem; --distance:8.511306848860492rem; --position:13.980422518134233%; --time:3.924674625328551s; --delay:-3.0579863461752197s;"></div>
        <div class="bubble" style="--size:5.611011784853356rem; --distance:6.2031218643026955rem; --position:4.245309746770378%; --time:3.7017366296252034s; --delay:-2.047406384981996s;"></div>
        <div class="bubble" style="--size:5.922616550346951rem; --distance:8.53635758032487rem; --position:73.16318174772307%; --time:3.410911235154967s; --delay:-3.2904633622289516s;"></div>
        <div class="bubble" style="--size:5.775029123173261rem; --distance:9.209583092234023rem; --position:81.20231688361517%; --time:2.969509476726371s; --delay:-2.4356037408747433s;"></div>
        <div class="bubble" style="--size:3.0672107033038865rem; --distance:6.162935483977043rem; --position:92.59650639778434%; --time:2.9274202883041203s; --delay:-3.490885298010012s;"></div>
        <div class="bubble" style="--size:4.259723829104296rem; --distance:6.171812564380009rem; --position:16.90910036448244%; --time:2.8555389246962712s; --delay:-2.4951156080955297s;"></div>
        <div class="bubble" style="--size:2.327686440470856rem; --distance:6.147237365682538rem; --position:42.18739846257773%; --time:3.6943160857857946s; --delay:-2.280767819838104s;"></div>
        <div class="bubble" style="--size:4.6780048912294605rem; --distance:8.32049613462811rem; --position:9.13501302287385%; --time:2.9536461368812983s; --delay:-3.257407182852762s;"></div>
        <div class="bubble" style="--size:3.3065226254827884rem; --distance:7.116378642839668rem; --position:23.441283383735904%; --time:3.4827725221732067s; --delay:-3.398392510619609s;"></div>
        <div class="bubble" style="--size:4.680831739033325rem; --distance:7.960402198332744rem; --position:91.04605365208461%; --time:3.051319156193896s; --delay:-3.4959144354140395s;"></div>
        <div class="bubble" style="--size:5.381969395279515rem; --distance:6.681642675132341rem; --position:83.88986065771407%; --time:3.3874104813736317s; --delay:-2.0728281644189464s;"></div>
        <div class="bubble" style="--size:3.054853430528505rem; --distance:7.782993422445886rem; --position:96.90304813898294%; --time:3.3322649502535455s; --delay:-3.4016275301436245s;"></div>
        <div class="bubble" style="--size:2.4676127508463797rem; --distance:9.271592855251821rem; --position:88.58765114937194%; --time:3.3507198433233607s; --delay:-3.468587829568211s;"></div>
        <div class="bubble" style="--size:2.48547936985322rem; --distance:8.150475109531033rem; --position:38.39388658000082%; --time:2.850632788372707s; --delay:-3.696171478371544s;"></div>
        <div class="bubble" style="--size:3.228065005310202rem; --distance:7.23657505331335rem; --position:41.05192849508653%; --time:3.212797819826079s; --delay:-3.989400638925042s;"></div>
        <div class="bubble" style="--size:4.277120568121929rem; --distance:7.65960122119686rem; --position:83.11452668483422%; --time:2.4930895792576413s; --delay:-3.8097052775868074s;"></div>
        <div class="bubble" style="--size:2.505993829776245rem; --distance:8.566351123104281rem; --position:58.499114530434184%; --time:3.4238339678583456s; --delay:-2.0244083763988887s;"></div>
        <div class="bubble" style="--size:4.81690788472417rem; --distance:9.488199428488631rem; --position:101.75803579912804%; --time:3.799631298580009s; --delay:-2.7261512902210687s;"></div>
        <div class="bubble" style="--size:3.0374447475471067rem; --distance:6.923610553118128rem; --position:53.24424023073501%; --time:2.7071754690999112s; --delay:-3.287424916177234s;"></div>
        <div class="bubble" style="--size:2.014213559496505rem; --distance:9.65568986130556rem; --position:47.25204254360977%; --time:3.3439813222935286s; --delay:-2.5733038457080184s;"></div>
        <div class="bubble" style="--size:5.029826709166106rem; --distance:9.083335067841848rem; --position:13.038130933701353%; --time:2.346110285153246s; --delay:-2.936751710545279s;"></div>
        <div class="bubble" style="--size:3.6512046792147084rem; --distance:9.219685942439984rem; --position:22.573143182879317%; --time:2.5423161492730766s; --delay:-2.338546080589235s;"></div>
        <div class="bubble" style="--size:3.7909780081878033rem; --distance:8.123930868025557rem; --position:68.80443223592563%; --time:2.388241318625191s; --delay:-2.4533392493248547s;"></div>
        <div class="bubble" style="--size:2.8514276701839325rem; --distance:7.214990261423268rem; --position:69.29451138159995%; --time:2.4040498310446337s; --delay:-3.0621659985262055s;"></div>
        <div class="bubble" style="--size:5.31147022485366rem; --distance:8.575742216336526rem; --position:88.73978309500112%; --time:2.0948685284969466s; --delay:-3.672203063988387s;"></div>
        <div class="bubble" style="--size:2.1373147124525183rem; --distance:6.550969596024323rem; --position:48.976060985576126%; --time:3.7763356543154223s; --delay:-2.657203624826978s;"></div>
        <div class="bubble" style="--size:4.7282352929104094rem; --distance:9.25242527320798rem; --position:31.692873101148308%; --time:3.740255441064562s; --delay:-2.123121861741112s;"></div>
        <div class="bubble" style="--size:4.827690791297298rem; --distance:6.999265441272583rem; --position:63.73792398007582%; --time:2.077341476387038s; --delay:-2.791508424383033s;"></div>
        <div class="bubble" style="--size:3.2354546257406778rem; --distance:8.710758461126307rem; --position:83.8773762250766%; --time:3.3360516509605294s; --delay:-3.392866335450785s;"></div>
        <div class="bubble" style="--size:3.6345598433863504rem; --distance:6.981338449022699rem; --position:85.27142606186636%; --time:3.8308746875345685s; --delay:-3.9870824274426377s;"></div>
        <div class="bubble" style="--size:3.2211753616946632rem; --distance:8.847036373412582rem; --position:72.58096849007899%; --time:3.3618804634700825s; --delay:-3.0569955039974954s;"></div>
        <div class="bubble" style="--size:4.879750294512973rem; --distance:7.490862549138049rem; --position:102.11694634852324%; --time:2.3807119341632723s; --delay:-3.9145536989709s;"></div>
        <div class="bubble" style="--size:4.73239727962054rem; --distance:7.834139910854936rem; --position:61.50978781279048%; --time:3.3838504241199923s; --delay:-2.2236552753455716s;"></div>
        <div class="bubble" style="--size:5.347131959551941rem; --distance:9.844977491562009rem; --position:24.194640266041894%; --time:3.7496315036770587s; --delay:-3.2181389405522247s;"></div>
        <div class="bubble" style="--size:5.727891286005207rem; --distance:9.089371056807202rem; --position:8.494928176471436%; --time:2.163173867574272s; --delay:-3.223377368519509s;"></div>
        <div class="bubble" style="--size:3.0375565478050026rem; --distance:7.11450796135264rem; --position:56.52810207610254%; --time:3.6068141514305156s; --delay:-3.7003132358612634s;"></div>
        <div class="bubble" style="--size:3.8973981987308495rem; --distance:6.715533996046205rem; --position:96.19949366166662%; --time:2.9032763743942107s; --delay:-3.1844935561800596s;"></div>
        <div class="bubble" style="--size:4.378335707276382rem; --distance:8.553239190511484rem; --position:25.127646186855607%; --time:2.356975980819081s; --delay:-3.8298259988493397s;"></div>
        <div class="bubble" style="--size:3.4054220290041233rem; --distance:6.877200387196548rem; --position:30.836868498429993%; --time:3.178177354436446s; --delay:-2.9870061241022383s;"></div>
        <div class="bubble" style="--size:4.005149082838103rem; --distance:8.323837479080462rem; --position:103.718830407681%; --time:3.286570740001913s; --delay:-3.637629874251127s;"></div>
        <div class="bubble" style="--size:5.093340249346092rem; --distance:8.311844124588603rem; --position:45.813800559118455%; --time:2.158007369890902s; --delay:-3.1754767783507054s;"></div>
        <div class="bubble" style="--size:5.920140491189328rem; --distance:7.83898024473812rem; --position:13.683586184762106%; --time:2.266134139943644s; --delay:-3.20881571231263s;"></div>
        <div class="bubble" style="--size:2.502618596892165rem; --distance:9.501778294433333rem; --position:49.47404647411597%; --time:2.7278694547170974s; --delay:-2.08393482241819s;"></div>
        <div class="bubble" style="--size:5.290057906916191rem; --distance:9.486542714859963rem; --position:83.73901532663484%; --time:3.022828988469637s; --delay:-3.1294480869784502s;"></div>
        <div class="bubble" style="--size:3.949773517821595rem; --distance:7.650128136876976rem; --position:54.49663381380388%; --time:2.9419907587576803s; --delay:-3.766728655204009s;"></div>
        <div class="bubble" style="--size:3.0194572723233293rem; --distance:6.22252448863143rem; --position:14.55909995285468%; --time:2.260538819131004s; --delay:-2.041581878028107s;"></div>
        <div class="bubble" style="--size:5.968571115218641rem; --distance:8.03784250238008rem; --position:52.20675083676261%; --time:2.9286874398295364s; --delay:-2.200697401820621s;"></div>
        <div class="bubble" style="--size:3.974310430147984rem; --distance:8.255895598877428rem; --position:85.30191013985187%; --time:3.0502671638084426s; --delay:-2.6956789617368586s;"></div>
        <div class="bubble" style="--size:2.751440494239998rem; --distance:9.531672736842731rem; --position:19.359482624375325%; --time:2.2462586808943583s; --delay:-3.808211982508144s;"></div>
        <div class="bubble" style="--size:2.6753210198227153rem; --distance:7.149831544358477rem; --position:71.41698489995527%; --time:3.2837470435210903s; --delay:-2.934636684266212s;"></div>
        <div class="bubble" style="--size:3.223402317145389rem; --distance:8.41040307965893rem; --position:92.15030351047062%; --time:2.5961206444261977s; --delay:-3.735881910095427s;"></div>
        <div class="bubble" style="--size:2.939822044882386rem; --distance:8.326431972813916rem; --position:58.756957775957574%; --time:2.3980746739012284s; --delay:-2.4134947233545527s;"></div>
        <div class="bubble" style="--size:2.7864194890555654rem; --distance:8.117575409353297rem; --position:-2.8778450354033502%; --time:3.402301117809994s; --delay:-3.753327928380177s;"></div>
        <div class="bubble" style="--size:2.4170562208361135rem; --distance:9.224571325838873rem; --position:22.41636407739772%; --time:3.747355504166693s; --delay:-2.621446296501955s;"></div>
        <div class="bubble" style="--size:2.4848349376485457rem; --distance:8.729906078856743rem; --position:3.7774719319762635%; --time:3.4261701221960514s; --delay:-3.5452190406218556s;"></div>
        <div class="bubble" style="--size:2.951136260484925rem; --distance:6.369859186448111rem; --position:90.51989261750262%; --time:3.9389009939451762s; --delay:-3.6416273007641764s;"></div>
        <div class="bubble" style="--size:5.35665797603955rem; --distance:6.655557716316423rem; --position:10.991959522101356%; --time:2.0828034939539664s; --delay:-3.5717189074529743s;"></div>
        <div class="bubble" style="--size:5.87655603081124rem; --distance:7.9890681774966055rem; --position:35.26978660477326%; --time:2.316884289013136s; --delay:-2.0709513389865575s;"></div>
        <div class="bubble" style="--size:3.8824998622418594rem; --distance:9.860815298697613rem; --position:18.944348027636817%; --time:3.5078756824821102s; --delay:-3.004925122581212s;"></div>
        <div class="bubble" style="--size:5.3508518186211855rem; --distance:9.415704034850691rem; --position:93.72745516168193%; --time:2.537891741403167s; --delay:-2.048913634728634s;"></div>
        <div class="bubble" style="--size:5.806796404022053rem; --distance:9.581141026350927rem; --position:41.15090657720761%; --time:3.8576985333343594s; --delay:-3.601018259536996s;"></div>
        <div class="bubble" style="--size:5.620351684432463rem; --distance:8.499659397926845rem; --position:6.414484080513558%; --time:3.29030564451431s; --delay:-2.9077367293971133s;"></div>
        <div class="bubble" style="--size:2.0365080000940132rem; --distance:9.610883030095355rem; --position:50.312462307132854%; --time:3.143813600570268s; --delay:-3.3950063291624013s;"></div>
        <div class="bubble" style="--size:4.742487697651595rem; --distance:8.809177247363426rem; --position:83.54229454351163%; --time:2.5472210010521557s; --delay:-3.5816314839397485s;"></div>
        <div class="bubble" style="--size:4.837846386993548rem; --distance:9.91980625167719rem; --position:14.00982174259947%; --time:2.094553674547811s; --delay:-2.292884354809948s;"></div>
        <div class="bubble" style="--size:5.665147669545902rem; --distance:7.321854404100106rem; --position:69.92716515672984%; --time:2.810933736348225s; --delay:-2.3375129426109433s;"></div>
        <div class="bubble" style="--size:2.03726210932348rem; --distance:9.375525447260317rem; --position:83.93564925622026%; --time:3.4379674633892114s; --delay:-3.451622202277885s;"></div>
        <div class="bubble" style="--size:4.141792552216006rem; --distance:7.943666601036521rem; --position:86.67990372453669%; --time:3.0468984089989157s; --delay:-2.6792414241234552s;"></div>
        <div class="bubble" style="--size:2.8751973676477833rem; --distance:6.211631960185014rem; --position:6.1762965236226%; --time:2.2333419419298832s; --delay:-3.0483216065472423s;"></div>
        <div class="bubble" style="--size:4.804404334240206rem; --distance:8.98529849092602rem; --position:89.49688902804776%; --time:3.6319531686868207s; --delay:-2.83478650620278s;"></div>
        <div class="bubble" style="--size:5.870803500547373rem; --distance:6.30940511575647rem; --position:24.21675228922384%; --time:2.9566679394618522s; --delay:-2.3484807210691074s;"></div>
        <div class="bubble" style="--size:2.58826754109111rem; --distance:8.067499901028192rem; --position:-3.68366718313637%; --time:3.9907914340286514s; --delay:-3.9606833525092124s;"></div>
        <div class="bubble" style="--size:4.89905297325446rem; --distance:8.80427309874122rem; --position:11.910804464571868%; --time:2.2207940894900657s; --delay:-2.914140535617866s;"></div>
        <div class="bubble" style="--size:3.4559441589404214rem; --distance:7.629161477849392rem; --position:54.73897717287723%; --time:3.5522590036974604s; --delay:-2.505954993379614s;"></div>
        <div class="bubble" style="--size:5.8314621211439315rem; --distance:6.782036557744254rem; --position:51.377552657511835%; --time:2.2672568737088183s; --delay:-2.2414276002911087s;"></div>
        <div class="bubble" style="--size:3.890267022247733rem; --distance:8.666420364255188rem; --position:63.30656086725084%; --time:2.561653483386975s; --delay:-3.8801196009886025s;"></div>
        <div class="bubble" style="--size:4.263136937435075rem; --distance:7.237925617569156rem; --position:93.8168114371118%; --time:3.1276112148431565s; --delay:-2.2812863156935963s;"></div>
        <div class="bubble" style="--size:5.22221826468126rem; --distance:7.143318956875766rem; --position:93.71630220346674%; --time:2.76980252387134s; --delay:-3.934884891494423s;"></div>
        <div class="bubble" style="--size:4.173058089229184rem; --distance:8.112418643263007rem; --position:1.0596193020716989%; --time:3.312810928683108s; --delay:-3.992503487599831s;"></div>
        <div class="bubble" style="--size:5.279649985761587rem; --distance:8.041581474759415rem; --position:41.22823052731618%; --time:3.0998612399726166s; --delay:-3.773821119514895s;"></div>
        <div class="bubble" style="--size:5.254783987144162rem; --distance:8.54794576824775rem; --position:96.67219026629024%; --time:2.5208025101108826s; --delay:-2.7795075904047293s;"></div>
        <div class="bubble" style="--size:2.8727770274620923rem; --distance:6.709038960730972rem; --position:62.87250973453621%; --time:3.9003904496001818s; --delay:-2.32077891358387s;"></div>
        <div class="bubble" style="--size:4.050182550049926rem; --distance:7.735168422987943rem; --position:64.8339919849536%; --time:2.865049275746584s; --delay:-3.480510579178446s;"></div>
        <div class="bubble" style="--size:2.4394892735312945rem; --distance:6.096308543408295rem; --position:43.314278811354185%; --time:3.8959389971249383s; --delay:-2.6921009666126454s;"></div>
        <div class="bubble" style="--size:5.603504323687251rem; --distance:8.71978428978577rem; --position:8.851893542899205%; --time:3.543682168919552s; --delay:-2.683195280701849s;"></div>
        <div class="bubble" style="--size:2.738988275976621rem; --distance:6.21098015778929rem; --position:13.773192653007811%; --time:2.2299519138143125s; --delay:-3.143702422019121s;"></div>
        <div class="bubble" style="--size:4.169178019920325rem; --distance:8.302355073266941rem; --position:62.29939188779372%; --time:3.2857007191918304s; --delay:-2.2259865150669245s;"></div>
        <div class="bubble" style="--size:4.828732515607931rem; --distance:8.628915789803855rem; --position:93.07746970742309%; --time:2.0621450654747s; --delay:-3.0536437979068074s;"></div>
        <div class="bubble" style="--size:4.510323110043392rem; --distance:9.25785301430973rem; --position:52.333710836647505%; --time:2.024631104235574s; --delay:-2.1894580405536224s;"></div>
        <div class="bubble" style="--size:4.1696083876151135rem; --distance:9.074693840677067rem; --position:103.47883303878461%; --time:2.667672974771185s; --delay:-3.9659126574430776s;"></div>
        <div class="bubble" style="--size:3.9015626151098246rem; --distance:8.125996131451018rem; --position:6.187058837655039%; --time:2.3432378116636943s; --delay:-2.625499816576004s;"></div>
        <div class="bubble" style="--size:4.970738302683282rem; --distance:8.956204568988621rem; --position:101.8852112537772%; --time:3.3776070975271817s; --delay:-2.786060851098883s;"></div>
        <div class="bubble" style="--size:4.447659150572274rem; --distance:7.255990818794476rem; --position:84.9092129327446%; --time:2.366704627566103s; --delay:-3.973038914855785s;"></div>
        <div class="bubble" style="--size:3.45427587522196rem; --distance:6.370613437423363rem; --position:47.56060016656575%; --time:3.5080801666570767s; --delay:-3.8708270380065533s;"></div>
        <div class="bubble" style="--size:4.344439850107776rem; --distance:8.73098270540029rem; --position:81.677021236691%; --time:3.545832785476115s; --delay:-2.053347574382802s;"></div>
        <div class="bubble" style="--size:3.4815007868374384rem; --distance:9.440268285874717rem; --position:2.052570937057368%; --time:3.668242955391324s; --delay:-2.945123371757641s;"></div>
        <div class="bubble" style="--size:2.5186746348668105rem; --distance:8.479050530825276rem; --position:40.52658702515365%; --time:3.9354207523886027s; --delay:-2.235477805531839s;"></div>
        <div class="bubble" style="--size:2.121058430761936rem; --distance:7.59599804831353rem; --position:25.316639756446115%; --time:3.803791707645295s; --delay:-3.6300171471977074s;"></div>
        <div class="bubble" style="--size:2.110563370090861rem; --distance:7.246026322315186rem; --position:33.6165913936069%; --time:2.372860962379807s; --delay:-3.560267852445009s;"></div>
        <div class="bubble" style="--size:2.500449076353922rem; --distance:9.875447332325368rem; --position:70.53241508837887%; --time:2.2301223974029587s; --delay:-2.5341129218064506s;"></div>
        <div class="bubble" style="--size:3.415918833977684rem; --distance:9.276583104278956rem; --position:4.371019395724787%; --time:3.72182204583198s; --delay:-3.331934306621536s;"></div>
        <div class="bubble" style="--size:3.3538475646433756rem; --distance:6.6420725990204605rem; --position:22.564361968013795%; --time:2.528398076608355s; --delay:-3.100793495199822s;"></div>
        <div class="bubble" style="--size:5.3336186895477615rem; --distance:8.713710762688041rem; --position:70.63627437871922%; --time:2.4650357125123716s; --delay:-2.365640555792074s;"></div>
        <div class="bubble" style="--size:5.529626825230714rem; --distance:7.145904314691052rem; --position:93.12432683218205%; --time:2.0418635982252975s; --delay:-3.0239351709288873s;"></div>
        <div class="bubble" style="--size:5.699172696018486rem; --distance:6.7634360183975915rem; --position:56.48767183653748%; --time:2.862395720235248s; --delay:-3.3790574837449157s;"></div>
        <div class="bubble" style="--size:5.781925285099987rem; --distance:6.687634674002349rem; --position:60.288048003159815%; --time:3.8390076911004654s; --delay:-3.4848615551390445s;"></div>
        <div class="bubble" style="--size:2.7363259689061374rem; --distance:6.099655433445572rem; --position:78.64363805491277%; --time:3.46862208768366s; --delay:-3.9822984979595706s;"></div>
        <div class="bubble" style="--size:2.036832459470542rem; --distance:9.716597591033869rem; --position:-4.34947702018847%; --time:2.2088162139369705s; --delay:-3.5552844731847397s;"></div>
        <div class="bubble" style="--size:4.365982194939446rem; --distance:7.130944296939659rem; --position:96.41582512011101%; --time:2.433269399283254s; --delay:-3.88584736928153s;"></div>
        <div class="bubble" style="--size:4.046758780083757rem; --distance:6.6296383661005365rem; --position:51.800026892462306%; --time:3.0031440668854352s; --delay:-2.964922785606162s;"></div>
        <div class="bubble" style="--size:3.7334483062530914rem; --distance:6.3662738837539745rem; --position:93.53569953210346%; --time:3.609593924279045s; --delay:-3.978586873114799s;"></div>
      </div>
      <div class="content row">

        <div class=" col-md-1 col-12 mt-5 ">
            <img src="{{ asset('img/game-of-thrones.jpg') }}" alt="profil" class="rounded-circle image-footer">
        </div>
            <div class="media-body col-md-3 col-12">
            <h5 class="mt-0">Media heading</h5>
                Cras sit amet nibh libero, in gravida nulla.  lacinia congue felis in faucibus.
                Cras sit amet nibh libero, in gravida nulla.  lacinia congue felis in faucibus.
                Cras sit amet nibh libero, in gravida nulla.  lacinia congue felis in faucibus.
            </div>

              <span class="col-md-6 col-12">
                  <h4>Category</h4><br>
                  <a href="#" class="mr-3"> lorem </a><a href="#" class="mr-3"> lorem </a><a href="#" class="mr-3"> lorem </a><a href="#" class="mr-3"> lorem </a><a href="#" class="mr-3"> lorem </a>
                  <a href="#" class="mr-3"> lorem </a><a href="#" class="mr-3"> lorem </a><a href="#" class="mr-3"> lorem </a><a href="#" class="mr-3"> lorem </a><a href="#" class="mr-3"> lorem </a>
                  <a href="#" class="mr-3"> lorem </a><a href="#" class="mr-3"> lorem </a><a href="#" class="mr-3"> lorem </a><a href="#" class="mr-3"> lorem </a><a href="#" class="mr-3"> lorem </a>
              </span>
              <div class="col-md-2 col-12 text-center copyright">
                <a class="image w-100 mt-5" href="https://codepen.io/z-" target="_blank" style="background-image: url(&quot;https://s3-us-west-2.amazonaws.com/s.cdpn.io/199011/happy.svg&quot;);"></a>
                <p>© <?= date('Y'); ?> Not Really</p>
              </div>
              {{-- <div class=""> --}}
                <ul class="col-12 social-network social-circle text-center mt-5">
                    <li><a class="icoLinkedin" href="/sosmed" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                    <li><a class="icoTwitter" href="/sosmed" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a class="icoMedium" href="/sosmed" title="Medium"><i class="fa fa-medium"></i></a></li>
                    <li><a class="icoQuora" href="/sosmed" title="Quora"><i class="fa fa-quora"></i></a></li>
                    <li><a class="icoFacebook" href="/sosmed" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="icoInstagram" href="/sosmed" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                </ul>
              {{-- </div> --}}
              
        </div>
      </div>
    </div>
    <svg style="position:fixed; top:100px;">
      <defs>
        <filter id="blob">
          <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur"></feGaussianBlur>
          <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="blob"></feColorMatrix>
          <feComposite in="SourceGraphic" in2="blob" operator="atop"></feComposite>
        </filter>
      </defs>
    </svg> -->



    <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>

    <script>
        $( () => {
            // $('.btn-category').mouseover( () => {

            //     $(this).toggleClass('active');
            // });

            // $('.btn-category').mouseout( () => {
            //     $('.category').css('display', 'none');
            //     $(this).toogleClass('active');
            // });


        })
    </script>
    </script>@yield('script')</script>


</body>

</html>
