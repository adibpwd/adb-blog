<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="{{ asset('css/test.css') }}">
    <link rel="stylesheet" href="{{ asset('js/test.js') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body><div class="header">
    <div class="splitview skewed rounded">
        <div class="panel bottom">
            <div class="content">
                <div class="description">
                    <h1>The original image.</h1>
                    <p>This is how the image looks like before applying a duotone effect.</p>
                </div>

                <img src="{{ asset('img/game-of-thrones.jpg') }}" alt="profil" class="rounded-circle image-footer"
                    style="filter: saturate(8); opacity: 0.5;">
            </div>
        </div>

        <div class="panel top">
            <div class="content row">
                {{-- <div class="row"> --}}

                    <div class="col-md-3">
                        <div class='walkthrough show reveal'>
                            <div class='walkthrough-pagination'>
                                <a class='dot active'></a>
                                <a class='dot'></a>
                                <a class='dot'></a>
                                <a class='dot'></a>
                                <a class='dot'></a>
                            </div>
                            <div class='walkthrough-body'>
                                <ul class='screens animate'>
                                    <li class='screen active'>
                                        <div class='media logo'>
                                            <img class='logo' src='https://s3.amazonaws.com/jebbles-codepen/icon.png'>
                                        </div>
                                        <h3>
                                            Product Intro
                                            <br>Walkthrough</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen'>
                                        <div class='media books'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/book_icon_1.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/book_icon_2.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/book_icon_3.png'>
                                        </div>
                                        <h3>
                                            Data and File
                                            <br>Management</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen'>
                                        <div class='media bars'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_axis.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_3.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_2.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_1.png'>
                                        </div>
                                        <h3>
                                            Analytics
                                            <br>and Metrics</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen'>
                                        <div class='media files'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_1.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_2.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_3.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_4.png'>
                                        </div>
                                        <h3>
                                            Reporting
                                            <br>and Insights</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen'>
                                        <div class='media comm'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/comm_icon_1.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/comm_icon_2.png'>
                                        </div>
                                        <h3>
                                            Communications
                                            <br>Tools</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                </ul>
                                <button class='prev-screen'>
                                    <i class="fas fa-angle-left"></i>
                                </button>
                                <button class='next-screen'>
                                    <i class="fas fa-angle-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class='walkthrough show reveal'>
                            <div class='walkthrough-pagination'>
                                <a class='dot active'></a>
                                <a class='dot'></a>
                                <a class='dot'></a>
                                <a class='dot'></a>
                                <a class='dot'></a>
                            </div>
                            <div class='walkthrough-body'>
                                <ul class='screens animate'>
                                    <li class='screen active'>
                                        <div class='media logo'>
                                            <img class='logo' src='https://s3.amazonaws.com/jebbles-codepen/icon.png'>
                                        </div>
                                        <h3>
                                            Product Intro
                                            <br>Walkthrough</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen'>
                                        <div class='media books'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/book_icon_1.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/book_icon_2.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/book_icon_3.png'>
                                        </div>
                                        <h3>
                                            Data and File
                                            <br>Management</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen'>
                                        <div class='media bars'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_axis.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_3.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_2.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_1.png'>
                                        </div>
                                        <h3>
                                            Analytics
                                            <br>and Metrics</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen'>
                                        <div class='media files'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_1.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_2.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_3.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_4.png'>
                                        </div>
                                        <h3>
                                            Reporting
                                            <br>and Insights</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen'>
                                        <div class='media comm'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/comm_icon_1.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/comm_icon_2.png'>
                                        </div>
                                        <h3>
                                            Communications
                                            <br>Tools</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                </ul>
                                <button class='prev-screen'>
                                    <i class="fas fa-angle-left"></i>
                                </button>
                                <button class='next-screen'>
                                    <i class="fas fa-angle-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class='walkthrough show reveal'>
                            <div class='walkthrough-pagination'>
                                <a class='dot active'></a>
                                <a class='dot'></a>
                                <a class='dot'></a>
                                <a class='dot'></a>
                                <a class='dot'></a>
                            </div>
                            <div class='walkthrough-body'>
                                <ul class='screens animate'>
                                    <li class='screen active'>
                                        <div class='media logo'>
                                            <img class='logo' src='https://s3.amazonaws.com/jebbles-codepen/icon.png'>
                                        </div>
                                        <h3>
                                            Product Intro
                                            <br>Walkthrough</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen'>
                                        <div class='media books'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/book_icon_1.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/book_icon_2.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/book_icon_3.png'>
                                        </div>
                                        <h3>
                                            Data and File
                                            <br>Management</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen'>
                                        <div class='media bars'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_axis.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_3.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_2.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_1.png'>
                                        </div>
                                        <h3>
                                            Analytics
                                            <br>and Metrics</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen'>
                                        <div class='media files'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_1.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_2.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_3.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_4.png'>
                                        </div>
                                        <h3>
                                            Reporting
                                            <br>and Insights</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen'>
                                        <div class='media comm'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/comm_icon_1.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/comm_icon_2.png'>
                                        </div>
                                        <h3>
                                            Communications
                                            <br>Tools</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                </ul>
                                <button class='prev-screen'>
                                    <i class="fas fa-angle-left"></i>
                                </button>
                                <button class='next-screen'>
                                    <i class="fas fa-angle-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class='walkthrough show reveal'>
                            <div class='walkthrough-pagination'>
                                <a class='dot active'></a>
                                <a class='dot'></a>
                                <a class='dot'></a>
                                <a class='dot'></a>
                                <a class='dot'></a>
                            </div>
                            <div class='walkthrough-body'>
                                <ul class='screens animate'>
                                    <li class='screen active'>
                                        <div class='media logo'>
                                            <img class='logo' src='https://s3.amazonaws.com/jebbles-codepen/icon.png'>
                                        </div>
                                        <h3>
                                            Product Intro
                                            <br>Walkthrough</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen'>
                                        <div class='media books'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/book_icon_1.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/book_icon_2.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/book_icon_3.png'>
                                        </div>
                                        <h3>
                                            Data and File
                                            <br>Management</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen'>
                                        <div class='media bars'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_axis.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_3.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_2.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_1.png'>
                                        </div>
                                        <h3>
                                            Analytics
                                            <br>and Metrics</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen'>
                                        <div class='media files'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_1.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_2.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_3.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_4.png'>
                                        </div>
                                        <h3>
                                            Reporting
                                            <br>and Insights</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen'>
                                        <div class='media comm'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/comm_icon_1.png'>
                                            <img class='icon' src='https://s3.amazonaws.com/jebbles-codepen/comm_icon_2.png'>
                                        </div>
                                        <h3>
                                            Communications
                                            <br>Tools</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                </ul>
                                <button class='prev-screen'>
                                    <i class="fas fa-angle-left"></i>
                                </button>
                                <button class='next-screen'>
                                    <i class="fas fa-angle-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>


                       

                {{-- </div> --}}


                <img src="{{ asset('img/game-of-thrones.jpg') }}" alt="profil" class="rounded-circle image-footer"
                    style="filter: saturate(1); opacity: 0.2;">
            </div>
        </div>

        <div class="handle"></div>
    </div>
</div>
</body>
</html>