<?php

use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 15; $i++) { 
            DB::table('posts')->insert([
                'title' => $i . $i . 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolor velit veniam',
                'content' => $i . 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolor velit veniam porro eius adipisci rerum asperiores sed nisi eum, pariatur, iure quaerat, doloremque autem inventore in sunt quod. Vitae, sit.',
                'tags' => 'bebas, lepas, tambak, sarirejo, ngaringan, grobogan',
                'author_id' => 3
            ]);
        }
    }
}
