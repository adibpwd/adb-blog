<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JD\Cloudder\Facades\Cloudder;
// Illuminate\Http\Request::getPathname;

class CloudderController extends Controller
{

    public function index() 
    {
        return view('cloudder');
    }

    public function upload(Request $request)
    {
        // dd(env('CLOUDINARY_API_SECRET'));

        $file = $request->file('file');
        $nameFile = $file->getClientOriginalName();
        // $pathFile = $file->getRealPath();
        // $sizeFile = $file->getSize();
        // $mimeFile = $file->getMimeType();

        \Cloudder::upload($file);
    }
}