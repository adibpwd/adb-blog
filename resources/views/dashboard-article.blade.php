@extends('layouts.foode')

{{-- {{ dd($user) }} --}}
{{-- {{ dd($allPost[11]['content']) }} --}}
{{-- {{ dd( htmlspecialchars_decode(stripslashes($allPost[11]['content'])) ) }} --}}

@section('head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
@endsection

@section('content')
<div class="container-fluid">
    <div class="btn-bg Ocean mt-4 mb-5">
        <div class="btn-group1 float-right">
            <div class="Coral1">
                <a href="/new post"><button>New Post<span class="Coralwave1"></span><span class="Coralwave2"></span><span class="Coralwave3"></span></button></a>
            </div>
        </div>
    </div>
</div>

@if ($post[0])
    <main>
        <ol class="gradient-list list-group">
            @for ($i = 0; $i < count($post); $i++) 
                <li
                    class="list-group-item d-flex justify-content-between align-items-center">
                    <a href="/single_post/{{ $post[$i]['slug'] }}" title="klik untuk melihat article">{{ $post[$i]['title'] }}</a>
                    {{-- <span class="badge badge-primary badge-pill">14</span> --}}
                    <span class="text-right">
                        <div class="btn-bg Ocean">
                            <div class="btn-group1">
                                <div class="Debris">
                                    <a href="/edit post/{{ $post[$i]['id'] }}"><button>Edit<span class="one"></span></button></a>
                                </div>
                                <div class="Coral">
                                    <a href="/delete/{{ $post[$i]['id'] }}"><button
                                            onclick="return confirm('kamu yakin menghapus post \n ( {{ $post[$i]['title'] }} ) ?\n'); ">Delete<span
                                                class="Coralwave1"></span><span class="Coralwave2"></span><span
                                                class="Coralwave3"></span></button></a>
                                </div>
                            </div>
                        </div>
                    </span>
                </li>
                @endfor
        </ol>
    </main>
@else
    <br><br><br><br>
        <h1 class="text-center">belum ada post..buat post sekarang klik new post!!</h1>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
@endif

{{ $post->links() }}  



@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
@endsection

@push('script')
<script>
    function confirm1() {
        $.confirm({
            title: 'konfirmasi delete',
            content: 'kamu yakin mau menghapus post ini?',
            buttons: {
                confirm: function () {
                    $.alert('Confirmed!');
                },
                cancel: function () {
                    $.alert('Canceled!');
                },
                somethingElse: {
                    text: 'Something else',
                    btnClass: 'btn-blue',
                    keys: ['enter', 'shift'],
                    action: function () {
                        $.alert('Something else?');
                    }
                }
            }
        });
    }

</script>
@endpush
