

@extends('layouts.foode')

@section('title', 'profile')

@section('head')
    	<!-- Font -->
        <meta name="csrf-token" content="{{ csrf_token() }}" />
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700%7CAllura" rel="stylesheet">
	
	<!-- Stylesheets -->
	
	<link href="{{ asset('css/ionicons.css') }}" rel="stylesheet">
	
	<link href="{{ asset('css/fluidbox.min.css') }}" rel="stylesheet">
	
	<link href="{{ asset('css/style2.css') }}" rel="stylesheet">
	
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Metal Mania' rel='stylesheet'>
    
@endsection

@section('author', $user['username'])
    
@show

@section('content')

    @if ($user == 'user not found')
        <img src="https://res.cloudinary.com/duh6epdw5/image/upload/v1590992795/usernotfound_emxrrw.jpg" alt="">
    @else
        @php
            $sosmed = ['facebook', 'twitter', 'instagram', 'linkedin', 'youtube'];
            $classP = ['p-item', 'p-item2', 'web-design', 'graphic-design', 'branding'];
            $pClass = [
                'p-item web-design',
                'p-item branding graphic-design',
                'p-item web-design',
                'p-item p-item-2 graphic-design',
                'p-item branding graphic-design',
                'p-item graphic-design web-design',
                'p-item  graphic-design branding',
                'p-item web-design branding',
                'p-item p-item-2 graphic-design'
            ];
            // pkai ini  menghemat kuota cuman ditampilan tidak enak dipandang tidak bisa pakai object-fit: cover;
            // $scaleImg = [
            //     'c_scale,w_600,h_400',
            //     'c_scale,w_600,h_800',
            //     'c_scale,w_600,h_400',
            //     'c_scale,w_300,h_400',
            //     'c_scale,w_600,h_400',
            //     'c_scale,w_600,h_400',
            //     'c_scale,w_600,h_800',
            //     'c_scale,w_600,h_800',
            //     'c_scale,w_300,h_400'
            // ];
            $imgTest = [
                'width: 600px; height: 400px;',
                'width: 600px; height: 800px;',
                'width: 600px; height: 400px;',
                'width: 300px; height: 400px;',
                'width: 600px; height: 400px;',
                'width: 600px; height: 400px;',
                'width: 600px; height: 800px;',
                'width: 600px; height: 800px;',
                'width: 300px; height: 400px;'
            ]
        @endphp

        {{-- {{ $age = date_diff(date_create($user['tgl_lahir']), date_create('now'))->y }} --}}
        <section class="intro-section" style="background-image: url(https://res.cloudinary.com/duh6epdw5/image/upload/{{ $user['foto_header'] }});">
            <div class="container">
                <div class="row">
                    <div class="col-md-1 col-lg-2"></div>
                    <div class="col-md-10 col-lg-8">
                        <div class="intro">
                            <div class="profile-img">
                                <img src="https://res.cloudinary.com/duh6epdw5/image/upload/{{ $user['foto_profil'] }}" alt="">
                                @if ( $user['id'] != Auth::user()->id )
                                    <form action="#" form>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <p class="target-follow" hidden>{{ $user['id']}}</p>
                                        <p class="id-user" hidden>{{ Auth::user()->id }}</p>
                                        <div style="position: absolute; bottom: 20px; right: 45px; left: 45px; background-color: white; border-radius: 15px; padding: 2px 10px;" class="foll">
                                            {{ $followers }} <span class="badge badge-primary" id="amount-followers" style="margin: 0!important;">{{ $amount_followers }}</span>
                                        </div>
                                    </form>
                                @endif
                            </div>
                            <h2><b>{{ $user['username'] }}</b></h2>
                            {{-- <h4 class="text-yellow">Key Account Manager</h4> --}}
                            <ul class="information margin-tb-30">
                                <li><b>Birthday :  </b> {{ date('j F Y', strtotime($user['tgl_lahir'])) }}</li>
                                <li><b>EMAIL : </b>{{ $user['email']}}</li>
                                {{-- <li><b>MARITAL STATUS : </b>Married</li> --}}
                            </ul>
                            <ul class="social-icons">
                                @if ($user['sosmed'])
                                    @for ($i = 0; $i < count($sosmed); $i++)
                                        <?php $sosmeds = $user['sosmed'][$sosmed[$i]]; ?>
                                        @if ($sosmeds)
                                            <li><a href="{{ $user['sosmed'][$sosmed[$i]] }}"><i class="ion-social-{{ $sosmed[$i] }}"></i></a></li>
                                        @else
                                            <li><a href="#"><i class="ion-social-{{ $sosmed[$i] }}"></i></a></li>
                                        @endif
                                    @endfor   
                                @else
                                    <p></p>
                                @endif
                                {{-- <li><a href="#"><i class="ion-social-instagram"></i></a></li>
                                <li><a href="#"><i class="ion-social-facebook"></i></a></li>
                                <li><a href="#"><i class="ion-social-twitter"></i></a></li> --}}
                            </ul>
                        </div><!-- intro -->
                    </div><!-- col-sm-8 -->
                </div><!-- row -->
            </div><!-- container -->
        </section>



        {{-- PORTFOLIO INDEX --}}

        <section class="portfolio-section section text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="heading">
                        <h3><b>All Post</b></h3>
                        <h6 class="text-black text-capitalize"><b>{{ count($posts) }} post ditampilkan</b></h6>
                    </div>
                </div><!-- col-sm-4 -->
            </div><!-- row -->
        </div><!-- container -->

        <div class="portfolioContainer">

            <?php $k = 0; $o = 1; $m = 3;?>
            @for ($i = 0; $i < count($pClass); $i++)
                @if ($posts[$k])
                    @if ($i == 0 or $i == 1 or $i == 2 or $i == 4 or $i == 5 or $i == 6 or $i == 7)
                        <div class="{{ $pClass[$i] }}">
                            <a href="#" class="img">
                                <img style="{{ $imgTest[$i] }} object-fit: cover!important;" src="https://res.cloudinary.com/duh6epdw5/image/upload/{{ $posts[$k]['thumbnail'] }}" alt="">
                            </a>
                            <div class="con-tooltip top text-center" style="z-index: 991;  position: absolute; bottom: 8px; left: 36px;">
                                <p class="m-1"> details </p>
                                <div class="tooltip" style="overflow-y: scroll; height: 200px;">
                                    <p>judul : {{ Str::limit($posts[$k]['title'], 100) }}</p>
                                    <hr>
                                    <p>kontent : {{ Str::limit(strip_tags($posts[$k]['content'], 300)) }}</p> <br>
                                    <a href="/single_post/{{ $posts[$k++]['slug'] }}">
                                        <svg class="liquid-button"
                                            data-text="Lanjut Baca"
                                            data-force-factor="0.1"
                                            data-layer-1-viscosity="0.5"
                                            data-layer-2-viscosity="0.4"
                                            data-layer-1-mouse-force="400"
                                            data-layer-2-mouse-force="500"
                                            data-layer-1-force-limit="1"
                                            data-layer-2-force-limit="2"
                                            data-color1="#4DE7BF"
                                            data-color2="#0A3FC7"
                                            data-color3="#2A62F4">
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @elseif ($i == 3 or $i == 8)
                        <div class="{{ $pClass[$i] }}">
                            @if ($posts[$k])
                                <a href="#" class="img">
                                    <img style="{{ $imgTest[$i] }} object-fit: cover!important;" src="https://res.cloudinary.com/duh6epdw5/image/upload/{{ $posts[$k]['thumbnail'] }}" alt="">
                                    <div class="con-tooltip top text-center" style="z-index: 991;  position: absolute; bottom: 8px; left: 36px;">
                                        <p class="m-1"> details </p>
                                        <div class="tooltip" style="overflow-y: scroll; height: 200px;">
                                            <p>judul : {{ Str::limit($posts[$k]['title'], 100) }}</p>
                                            <hr>
                                            <p>kontent : {{ Str::limit(strip_tags($posts[$k]['content'], 300)) }}</p> <br>
                                            {{-- <button> --}}
                                                {{-- < href="/single_post/{{ $posts[$i]['title'] }}"> --}}
                                                <svg class="liquid-button" id="link{{ $m }}"
                                                    data-text="Lanjut Baca"
                                                    data-force-factor="0.1"
                                                    data-layer-1-viscosity="0.5"
                                                    data-layer-2-viscosity="0.4"
                                                    data-layer-1-mouse-force="400"
                                                    data-layer-2-mouse-force="500"
                                                    data-layer-1-force-limit="1"
                                                    data-layer-2-force-limit="2"
                                                    data-color1="#4DE7BF"
                                                    data-color2="#0A3FC7"
                                                    data-color3="#2A62F4" value="testing gan">
                                                    <b id="link0{{ $m++ }}" hidden>/single_post/{{ $posts[$k++]['slug'] }}</b>
                                                </svg>
                                            {{-- </button> --}}
                                        </div>
                                    </div>
                                </a>
                            @else
                                <a href="#" class="img">
                                    <img style="{{ $imgTest[$i] }} object-fit: cover!important;" src="https://res.cloudinary.com/duh6epdw5/image/upload/v1590960537/samples/image-x-png_csfjwr.png" alt="">
                                </a>
                            @endif
                            @if ($posts[$k])
                                <a href="#" class="img">
                                    <img style="{{ $imgTest[$i] }} object-fit: cover!important;" src="https://res.cloudinary.com/duh6epdw5/image/upload/{{ $posts[$k]['thumbnail'] }}" alt="">
                                    <div class="con-tooltip top text-center" style="z-index: 991;  position: absolute; bottom: 8px; right: 36px;">
                                        <p class="m-1"> details </p>
                                        <div class="tooltip" style="overflow-y: scroll; height: 200px; left: -239%;">
                                            <p>judul : {{ Str::limit($posts[$k]['title'], 100) }}</p>
                                            <hr>
                                            <p>kontent : {{ Str::limit(strip_tags($posts[$k]['content'], 300)) }}</p> <br>
                                            {{-- <button> --}}
                                                {{-- < href="/single_post/{{ $posts[$i]['title'] }}"> --}}
                                                <svg class="liquid-button" id="link{{ $o }}"
                                                    data-text="Lanjut Baca"
                                                    data-force-factor="0.1"
                                                    data-layer-1-viscosity="0.5"
                                                    data-layer-2-viscosity="0.4"
                                                    data-layer-1-mouse-force="400"
                                                    data-layer-2-mouse-force="500"
                                                    data-layer-1-force-limit="1"
                                                    data-layer-2-force-limit="2"
                                                    data-color1="#4DE7BF"
                                                    data-color2="#0A3FC7"
                                                    data-color3="#2A62F4" value="testing gan">
                                                    <b id="link0{{ $o++ }}" hidden>/single_post/{{ $posts[$k++]['slug'] }}</b>
                                                </svg>
                                            {{-- </button> --}}
                                        </div>
                                    </div>
                                </a>
                            @else
                                <a href="#" class="img">
                                    <img style="{{ $imgTest[$i] }} object-fit: cover!important;" src="https://res.cloudinary.com/duh6epdw5/image/upload/v1590960537/samples/image-x-png_csfjwr.png" alt="">
                                </a>
                            @endif
                        </div>                
                    @endif
                @else
                    @if ($i == 0 or $i == 1 or $i == 2 or $i == 4 or $i == 5 or $i == 6 or $i == 7)
                        <div class="{{ $pClass[$i] }}">
                            <a href="#" class="img">
                                <img style="{{ $imgTest[$i] }} object-fit: cover!important;" src="https://res.cloudinary.com/duh6epdw5/image/upload/v1590960537/samples/image-x-png_csfjwr.png" alt="">
                            </a>
                        </div>
                    @elseif ($i == 3 or $i == 8)
                        <div class="{{ $pClass[$i] }}">
                            <a href="#" class="img">
                                <img style="{{ $imgTest[$i] }} object-fit: cover!important;" src="https://res.cloudinary.com/duh6epdw5/image/upload/v1590960537/samples/image-x-png_csfjwr.png" alt="">
                            </a>
                            <a href="#" class="img">
                                <img style="{{ $imgTest[$i] }} object-fit: cover!important;" src="https://res.cloudinary.com/duh6epdw5/image/upload/v1590960537/samples/image-x-png_csfjwr.png" alt="">
                            </a>
                        </div>                
                    @endif
                @endif
            @endfor

        </div><!-- portfolioContainer -->

        </section><!-- portfolio-section -->
        <br><br>
        {{ $posts->links() }}
    @endif

    <br>
    <div class="htmltest">

    </div>

@endsection

@section('script')
    	<!-- SCIPTS -->
	
	
	{{-- <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script> --}}
	<script src="{{ asset('js/tether.min.js') }}"></script>
	
	<script src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
	
	<script src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
	
	<script src="{{ asset('js/progressbar.min.js') }}"></script>
	
    <script src="{{ asset('js/jquery.fluidbox.min.js') }}"></script>
    
	
	{{-- <script src="{{ asset('js/scripts.js') }}"></script> --}}
@endsection

@push('script')
    <script>
        $( () => {
            
            (function ($) {

            "use strict";




            // LINE PROGRESS BAR
            enableLineProgress();

            // RADIAL PROGRESS BAR
            enableRadialProgress();

            // ACCORDIAN
            panelAccordian();

            $(window).on('load', function(){
                
                // ISOTOPE PORTFOLIO WITH FILTER
                if(isExists('.portfolioContainer')){
                    var $container = $('.portfolioContainer');
                    $container.isotope({
                        filter: '*',
                        animationOptions: {
                            duration: 750,
                            easing: 'linear',
                            queue: false
                        }
                    });
                
                    $('.portfolioFilter a').click(function(){
                        $('.portfolioFilter .current').removeClass('current');
                        $(this).addClass('current');
                
                        var selector = $(this).attr('data-filter');
                        $container.isotope({
                            filter: selector,
                            animationOptions: {
                                duration: 750,
                                easing: 'linear',
                                queue: false
                            }
                        });
                        return false;
                    }); 
                }

            });


            $('a[href="#"]').on('click', function(event){
                return;
            });


            if ( $.isFunction($.fn.fluidbox) ) {
                $('a').fluidbox();
            }

            var countCounterUp = 0;

            var a = 0 ;

            countCounterUp = enableCounterUp(countCounterUp);

            $(window).on('scroll', function(){
                
                countCounterUp = enableCounterUp(countCounterUp);

            });


            })(jQuery);

            function panelAccordian(){

            var panelTitle = $('.panel-title');
            panelTitle.on('click', function(){
                $('.panel-title').removeClass('active');
                $(this).toggleClass('active');
                
            });

            }

            function enableRadialProgress(){

            $(".radial-progress").each(function(){
                var $this = $(this),
                    progPercent = $this.data('prog-percent');
                    
                var bar = new ProgressBar.Circle(this, {
                    color: '#aaa',
                    strokeWidth: 3,
                    trailWidth: 1,
                    easing: 'easeInOut',
                    duration: 1400,
                    text: {
                        
                    },
                    from: { color: '#aaa', width: 1 },
                    to: { color: '#FEAE01', width: 3 },
                    // Set default step function for all animate calls
                    step: function(state, circle) {
                        circle.path.setAttribute('stroke', state.color);
                        circle.path.setAttribute('stroke-width', state.width);

                        var value = Math.round(circle.value() * 100);
                        if (value === 0) {
                            circle.setText('');
                        } else {
                            circle.setText(value);
                        }

                    }
                });
                
                $(this).waypoint(function(){
                bar.animate(progPercent);  
                },{offset: "90%"})
                
            });
            }

            function enableLineProgress(){

            $(".line-progress").each(function(){
                var $this = $(this),
                    progPercent = $this.data('prog-percent');
                    
                var bar = new ProgressBar.Line(this, {
                    strokeWidth: 1,
                    easing: 'easeInOut',
                    duration: 1400,
                    color: '#FEAE01',
                    trailColor: '#eee',
                    trailWidth: 1,
                    svgStyle: {width: '100%', height: '100%'},
                    text: {
                        style: {
                            
                        },
                    },
                    from: {color: '#FFEA82'},
                    to: {color: '#ED6A5A'},
                    step: (state, bar) => {
                        bar.setText(Math.round(bar.value() * 100) + ' %');
                    }
                });
                
                $(this).waypoint(function(){
                bar.animate(progPercent);  
                },{offset: "90%"})
                
            });
            }

            function enableCounterUp(a){

            var counterElement;

            if(isExists('#counter')){ counterElement = $('#counter'); }
            else{ return; }
                
            var oTop = $('#counter').offset().top - window.innerHeight;
            if (a == 0 && $(window).scrollTop() > oTop) {
                $('.counter-value').each(function() {
                    var $this = $(this),
                        countDuration = $this.data('duration'),
                        countTo = $this.attr('data-count');
                    $({
                        countNum: $this.text()
                    }).animate({
                        countNum: countTo
                    },{

                        duration: countDuration,
                        easing: 'swing',
                        step: function() {
                            $this.text(Math.floor(this.countNum));
                        },
                        complete: function() {
                            $this.text(this.countNum);
                        }

                    });
                });
                a = 1;
            }

            return a;
            }

            function isExists(elem){
            if ($(elem).length > 0) { 
                return true;
            }
            return false;
            }

            const LiquidButton = class LiquidButton {
            constructor(svg) {
                const options = svg.dataset;
                this.id = this.constructor.id || (this.constructor.id = 1);
                this.constructor.id++;
                this.xmlns = 'http://www.w3.org/2000/svg';
                this.tension = options.tension * 1 || 0.4;
                this.width = options.width * 1 || 200;
                this.height = options.height * 1 || 50;
                this.margin = options.margin || 40;
                this.hoverFactor = options.hoverFactor || -0.1;
                this.gap = options.gap || 5;
                this.debug = options.debug || false;
                this.forceFactor = options.forceFactor || 0.2;
                this.color1 = options.color1 || '#36DFE7';
                this.color2 = options.color2 || '#8F17E1';
                this.color3 = options.color3 || '#BF09E6';
                this.textColor = options.textColor || '#FFFFFF';
                this.text = options.text || 'Button';
                this.svg = svg;
                this.layers = [{
                points: [],
                viscosity: 0.5,
                mouseForce: 100,
                forceLimit: 2 },
                {
                points: [],
                viscosity: 0.8,
                mouseForce: 150,
                forceLimit: 3 }];

                for (let layerIndex = 0; layerIndex < this.layers.length; layerIndex++) {
                const layer = this.layers[layerIndex];
                layer.viscosity = options['layer-' + (layerIndex + 1) + 'Viscosity'] * 1 || layer.viscosity;
                layer.mouseForce = options['layer-' + (layerIndex + 1) + 'MouseForce'] * 1 || layer.mouseForce;
                layer.forceLimit = options['layer-' + (layerIndex + 1) + 'ForceLimit'] * 1 || layer.forceLimit;
                layer.path = document.createElementNS(this.xmlns, 'path');
                this.svg.appendChild(layer.path);
                }
                this.wrapperElement = options.wrapperElement || document.body;
                if (!this.svg.parentElement) {
                this.wrapperElement.append(this.svg);
                }

                this.svgText = document.createElementNS(this.xmlns, 'text');
                this.svgText.setAttribute('x', '50%');
                this.svgText.setAttribute('y', '30%');
                this.svgText.setAttribute('dy', ~~(this.height / 8) + 'px');
                this.svgText.setAttribute('font-size', ~~(this.height / 3));
                this.svgText.style.fontFamily = 'sans-serif';
                this.svgText.setAttribute('text-anchor', 'middle');
                this.svgText.setAttribute('pointer-events', 'none');
                this.svg.appendChild(this.svgText);

                this.svgDefs = document.createElementNS(this.xmlns, 'defs');
                this.svg.appendChild(this.svgDefs);

                this.touches = [];
                this.noise = options.noise || 0;
                document.body.addEventListener('touchstart', this.touchHandler);
                document.body.addEventListener('touchmove', this.touchHandler);
                document.body.addEventListener('touchend', this.clearHandler);
                document.body.addEventListener('touchcancel', this.clearHandler);
                this.svg.addEventListener('mousemove', this.mouseHandler);
                this.svg.addEventListener('mouseout', this.clearHandler);
                this.initOrigins();
                this.animate();
            }

            get mouseHandler() {
                return e => {
                this.touches = [{
                    x: e.offsetX,
                    y: e.offsetY,
                    force: 1 }];

                };
            }

            get touchHandler() {
                return e => {
                this.touches = [];
                const rect = this.svg.getBoundingClientRect();
                for (let touchIndex = 0; touchIndex < e.changedTouches.length; touchIndex++) {
                    const touch = e.changedTouches[touchIndex];
                    const x = touch.pageX - rect.left;
                    const y = touch.pageY - rect.top;
                    if (x > 0 && y > 0 && x < this.svgWidth && y < this.svgHeight) {
                    this.touches.push({ x, y, force: touch.force || 1 });
                    }
                }
                e.preventDefault();
                };
            }

            get clearHandler() {
                return e => {
                this.touches = [];
                };
            }

            get raf() {
                return this.__raf || (this.__raf = (
                window.requestAnimationFrame ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                function (callback) {setTimeout(callback, 10);}).
                bind(window));
            }

            distance(p1, p2) {
                return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
            }

            update() {
                for (let layerIndex = 0; layerIndex < this.layers.length; layerIndex++) {
                const layer = this.layers[layerIndex];
                const points = layer.points;
                for (let pointIndex = 0; pointIndex < points.length; pointIndex++) {
                    const point = points[pointIndex];
                    const dx = point.ox - point.x + (Math.random() - 0.5) * this.noise;
                    const dy = point.oy - point.y + (Math.random() - 0.5) * this.noise;
                    const d = Math.sqrt(dx * dx + dy * dy);
                    const f = d * this.forceFactor;
                    point.vx += f * (dx / d || 0);
                    point.vy += f * (dy / d || 0);
                    for (let touchIndex = 0; touchIndex < this.touches.length; touchIndex++) {
                    const touch = this.touches[touchIndex];
                    let mouseForce = layer.mouseForce;
                    if (
                    touch.x > this.margin &&
                    touch.x < this.margin + this.width &&
                    touch.y > this.margin &&
                    touch.y < this.margin + this.height)
                    {
                        mouseForce *= -this.hoverFactor;
                    }
                    const mx = point.x - touch.x;
                    const my = point.y - touch.y;
                    const md = Math.sqrt(mx * mx + my * my);
                    const mf = Math.max(-layer.forceLimit, Math.min(layer.forceLimit, mouseForce * touch.force / md));
                    point.vx += mf * (mx / md || 0);
                    point.vy += mf * (my / md || 0);
                    }
                    point.vx *= layer.viscosity;
                    point.vy *= layer.viscosity;
                    point.x += point.vx;
                    point.y += point.vy;
                }
                for (let pointIndex = 0; pointIndex < points.length; pointIndex++) {
                    const prev = points[(pointIndex + points.length - 1) % points.length];
                    const point = points[pointIndex];
                    const next = points[(pointIndex + points.length + 1) % points.length];
                    const dPrev = this.distance(point, prev);
                    const dNext = this.distance(point, next);

                    const line = {
                    x: next.x - prev.x,
                    y: next.y - prev.y };

                    const dLine = Math.sqrt(line.x * line.x + line.y * line.y);

                    point.cPrev = {
                    x: point.x - line.x / dLine * dPrev * this.tension,
                    y: point.y - line.y / dLine * dPrev * this.tension };

                    point.cNext = {
                    x: point.x + line.x / dLine * dNext * this.tension,
                    y: point.y + line.y / dLine * dNext * this.tension };

                }
                }
            }

            animate() {
                this.raf(() => {
                this.update();
                this.draw();
                this.animate();
                });
            }

            get svgWidth() {
                return this.width + this.margin * 2;
            }

            get svgHeight() {
                return this.height + this.margin * 2;
            }

            draw() {
                for (let layerIndex = 0; layerIndex < this.layers.length; layerIndex++) {
                const layer = this.layers[layerIndex];
                if (layerIndex === 1) {
                    if (this.touches.length > 0) {
                    while (this.svgDefs.firstChild) {
                        this.svgDefs.removeChild(this.svgDefs.firstChild);
                    }
                    for (let touchIndex = 0; touchIndex < this.touches.length; touchIndex++) {
                        const touch = this.touches[touchIndex];
                        const gradient = document.createElementNS(this.xmlns, 'radialGradient');
                        gradient.id = 'liquid-gradient-' + this.id + '-' + touchIndex;
                        const start = document.createElementNS(this.xmlns, 'stop');
                        start.setAttribute('stop-color', this.color3);
                        start.setAttribute('offset', '0%');
                        const stop = document.createElementNS(this.xmlns, 'stop');
                        stop.setAttribute('stop-color', this.color2);
                        stop.setAttribute('offset', '100%');
                        gradient.appendChild(start);
                        gradient.appendChild(stop);
                        this.svgDefs.appendChild(gradient);
                        gradient.setAttribute('cx', touch.x / this.svgWidth);
                        gradient.setAttribute('cy', touch.y / this.svgHeight);
                        gradient.setAttribute('r', touch.force);
                        layer.path.style.fill = 'url(#' + gradient.id + ')';
                    }
                    } else {
                    layer.path.style.fill = this.color2;
                    }
                } else {
                    layer.path.style.fill = this.color1;
                }
                const points = layer.points;
                const commands = [];
                commands.push('M', points[0].x, points[0].y);
                for (let pointIndex = 1; pointIndex < points.length; pointIndex += 1) {
                    commands.push('C',
                    points[(pointIndex + 0) % points.length].cNext.x,
                    points[(pointIndex + 0) % points.length].cNext.y,
                    points[(pointIndex + 1) % points.length].cPrev.x,
                    points[(pointIndex + 1) % points.length].cPrev.y,
                    points[(pointIndex + 1) % points.length].x,
                    points[(pointIndex + 1) % points.length].y);

                }
                commands.push('Z');
                layer.path.setAttribute('d', commands.join(' '));
                }
                this.svgText.textContent = this.text;
                this.svgText.style.fill = this.textColor;
            }

            createPoint(x, y) {
                return {
                x: x,
                y: y,
                ox: x,
                oy: y,
                vx: 0,
                vy: 0 };

            }

            initOrigins() {
                this.svg.setAttribute('width', this.svgWidth);
                this.svg.setAttribute('height', this.svgHeight);
                for (let layerIndex = 0; layerIndex < this.layers.length; layerIndex++) {
                const layer = this.layers[layerIndex];
                const points = [];
                for (let x = ~~(this.height / 2); x < this.width - ~~(this.height / 2); x += this.gap) {
                    points.push(this.createPoint(
                    x + this.margin,
                    this.margin));

                }
                for (let alpha = ~~(this.height * 1.25); alpha >= 0; alpha -= this.gap) {
                    const angle = Math.PI / ~~(this.height * 1.25) * alpha;
                    points.push(this.createPoint(
                    Math.sin(angle) * this.height / 2 + this.margin + this.width - this.height / 2,
                    Math.cos(angle) * this.height / 2 + this.margin + this.height / 2));

                }
                for (let x = this.width - ~~(this.height / 2) - 1; x >= ~~(this.height / 2); x -= this.gap) {
                    points.push(this.createPoint(
                    x + this.margin,
                    this.margin + this.height));

                }
                for (let alpha = 0; alpha <= ~~(this.height * 1.25); alpha += this.gap) {
                    const angle = Math.PI / ~~(this.height * 1.25) * alpha;
                    points.push(this.createPoint(
                    this.height - Math.sin(angle) * this.height / 2 + this.margin - this.height / 2,
                    Math.cos(angle) * this.height / 2 + this.margin + this.height / 2));

                }
                layer.points = points;
                }
            }};



            const redraw = () => {
            button.initOrigins();
            };

            const buttons = document.getElementsByClassName('liquid-button');
            for (let buttonIndex = 0; buttonIndex < buttons.length; buttonIndex++) {
            const button = buttons[buttonIndex];
            button.liquidButton = new LiquidButton(button);
            }

            $('#link1').click( () => {
                let url = $('#link01').text();
                // var pathname = window.location.pathname;
                console.log(url);
                
                window.location.replace(url);
            });
            $('#link2').click( () => {
                let url = $('#link02').text();
                // var pathname = window.location.pathname;
                console.log(url);
                
                window.location.replace(url);
            });
            $('#link3').click( () => {
                let url = $('#link03').text();
                // var pathname = window.location.pathname;
                console.log(url);
                
                window.location.replace(url);
            });
            $('#link4').click( () => {
                let url = $('#link04').text();
                // var pathname = window.location.pathname;
                console.log(url);
                
                window.location.replace(url);
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
            });

            $('.foll').click( () => {
                let foll = $('.foll').text();
                let idUser = $('.id-user').text();
                let targetFollow = $('.target-follow').text();
                followers = (amountFollowers) => {
                    return '<p class="badge badge-primary" id="amount-followers" style="margin: 0 5px!important;">' + amountFollowers + '</p>'
                }
                // let foll = foll.replace(/[0-9]/g, '');
                let foll1 = foll.replace(/[0-9]/g, '');
                let foll2 = foll1.replace(/\s/g, '');
                console.log(foll2);
                
                if(foll2 == 'follow') {
                    $('.foll').html('followed');
                    $.post('/foll', { 'followed':targetFollow, 'following':idUser}, function(data){
                        $('.foll').append(followers(data));
                    });
                } else {
                    $('.foll').html('follow');
                    $.post('/unfoll', { 'followed':targetFollow, 'following':idUser}, function(data){
                        $('.foll').append(followers(data));
                    });
                }
            });
        });
    </script>
@endpush