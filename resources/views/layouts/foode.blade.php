<!DOCTYPE html>
<html lang="en">

@yield('html')

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://kit.fontawesome.com/bd2a7f64b3.js" crossorigin="anonymous"></script>
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>@yield('title')</title>

    <!-- Favicon -->
    <link rel="icon" href=" {{ asset('img/core-img/favicon.ico') }}">

    @yield('head')

    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="{{ asset('/css/style1.css') }}">

    @stack('style')

</head>

<body>
    @php
        $categories = categories();
        $divCategory = ceil(count($categories) / 4); // division category
        $modCategory =  4 - (count($categories) % 4); // modulus category untuk mengurangi jumlah terakhrir perulangan biar $ nggak offset
        // dd(auth('username'));
    @endphp

    @stack('php')

    <!-- ##### Preloader ##### -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="circle-preloader text-center">
            <h1>my blog</h1>
            <div class="foode-preloader w-100">
                <span></span>
            </div>
            <h6>refresh ulang jika loading lama</h6>
        </div>
    </div>

            <!-- Logo Area -->
            <div class="logo-area text-center">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <!-- Nav brand -->
                            <a href="/profil/@yield('author')"  class="nav-brand"><h1 style="font-family: 'Metal Mania';">@yield('author')</h1></a>
                        </div>
                    </div>
                </div>
            </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area">

        <!-- Top Header Area -->
        {{-- <div class="top-header">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="top-header-content d-flex align-items-center justify-content-between">
                            <!-- Search Form -->
                            <div class="search-form">
                                <form action="#" method="get">
                                    <input type="search" name="search" class="form-control" placeholder="Search and hit enter...">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </form>
                            </div>

                            <!-- Social Button -->
                            <div class="top-social-info">
                                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-rss" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}



        <!-- Navbar Area -->
        <div class="foode-main-menu">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar" id="foodeNav">
                        
                        
                        <!-- Nav brand -->
                        <a href="/profil/@yield('author')" class="nav-brand">
                            <svg viewBox="0 0 2100 300" class="nav-logo">
                                <!-- Symbol-->
                                <symbol id="s-text">
                                  <text text-anchor="middle" x="50%" y="50%" dy=".35em">@yield('author')</text>
                                </symbol>
                                <!-- Duplicate symbols-->
                                <use class="text" xlink:href="#s-text"></use>
                                <use class="text" xlink:href="#s-text"></use>
                                <use class="text" xlink:href="#s-text"></use>
                                <use class="text" xlink:href="#s-text"></use>
                                <use class="text" xlink:href="#s-text"></use>
                            </svg>
                        </a>


                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">

                            <!-- close btn -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li><a href="/">Home</a></li>
                                    <li><a href="#">Catagories</a>
                                        <div class="megamenu d-flex justify-content-center">
                                            @for ($i = 1; $i <= $divCategory; $i++)
                                                @php
                                                    if ($i == 1) {
                                                        $k = 4; // amount @for @endfor category
                                                        $m = $k - 4; // start @for @endfor category
                                                    } elseif ($i == $divCategory ) {
                                                        $k = $i * 4;
                                                        $m = $k - 4;
                                                        $k = $k - $modCategory; // last amoun @for @endfor category
                                                    } else {
                                                        $k = $i * 4;
                                                        $m = $k - 4;
                                                    }
                                                @endphp
                                                {{-- {{ dd($k) }} --}}
                                                <ul class="single-mega cn-col-4">
                                                    @for ($l = $m; $l < $k; $l++)
                                                        <li><a href="/category/{{ $categories[$l]['category_name'] }} ">{{ $categories[$l]['category_name'] }}</a></li>
                                                    @endfor
                                                </ul>
                                            @endfor
                                        </div>
                                    </li>
                                    {{-- @if (Auth::user())
                                        
                                    @endif --}}
                                    @isset(Auth::user()->username)
                                        <li>
                                            <a href="#">Account</a>
                                            <div class="megamenu d-flex justify-content-center">
                                                <ul class="single-mega cn-col-4">
                                                    <li><a href="/dashboard/{{ Auth::user()->username }}">Dashboard</a></li>
                                                    <li><a href="/profil/{{ Auth::user()->username }}">My Profile</a></li>
                                                    <li><a href="/edit_profil/{{ Auth::user()->username }}">edit profile</a></li>
                                                    <li><a class="dropdown-item" href="{{ route('logout') }}"
                                                        onclick="event.preventDefault();
                                                                    document.getElementById('logout-form').submit();">
                                                        {{ __('Logout') }}
                                                    </a></li>
                                                    
                                                </ul>
                                            </div>
                                        </li>
                                        {{-- <li><a href="/profil/{{ Auth::user()->username }} ">my profile</a></li> --}}
                                        
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </div>
                                    @endisset
                                    @empty(Auth::user()->username)
                                        <li><a href="/user/login">login</a></li>
                                        <li><a href="/user/register">register</a></li>
                                    @endempty
                                    {{-- <li><a href="contact.html">Contact</a></li> --}}
                                </ul>

                            </div>
                            <!-- Nav End -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ##### Header Area End ##### -->

    @yield('content')

    <!-- ##### Instagram Area Start ##### -->
    {{-- <div class="follow-us-instagram">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                        <h2>Follow Us On Instagram</h2>
                        <span>@foodeblog</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="instagram-slides owl-carousel">
                        <!-- Single Instagram Slide -->
                        <div class="single-instagram-slide">
                            <img src=" {{ asset('img/blog-img/insta1.jpg') }}" alt="">
                            <a href="#"><i class="fa fa-instagram"></i>Follow Me</a>
                        </div>

                        <!-- Single Instagram Slide -->
                        <div class="single-instagram-slide">
                            <img src=" {{ asset('img/blog-img/insta2.jpg') }}" alt="">
                            <a href="#"><i class="fa fa-instagram"></i>Follow Me</a>
                        </div>

                        <!-- Single Instagram Slide -->
                        <div class="single-instagram-slide">
                            <img src=" {{ asset('img/blog-img/insta3.jpg') }}" alt="">
                            <a href="#"><i class="fa fa-instagram"></i>Follow Me</a>
                        </div>

                        <!-- Single Instagram Slide -->
                        <div class="single-instagram-slide">
                            <img src=" {{ asset('img/blog-img/insta4.jpg') }}" alt="">
                            <a href="#"><i class="fa fa-instagram"></i>Follow Me</a>
                        </div>

                        <!-- Single Instagram Slide -->
                        <div class="single-instagram-slide">
                            <img src=" {{ asset('img/blog-img/insta5.jpg') }}" alt="">
                            <a href="#"><i class="fa fa-instagram"></i>Follow Me</a>
                        </div>

                        <!-- Single Instagram Slide -->
                        <div class="single-instagram-slide">
                            <img src=" {{ asset('img/blog-img/insta6.jpg') }}" alt="">
                            <a href="#"><i class="fa fa-instagram"></i>Follow Me</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Instagram Area End ##### -->

    <!-- ##### Footer Area Start ##### -->
    <footer class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Footer Curve Line -->
                    <img class="footer-curve" src=" {{ asset('img/core-img/foo-curve.png') }}" alt="">
                    <!-- Footer Social Info -->
                    <div class="footer-social-info d-flex align-items-center justify-content-between">
                        <a href="#"><i class="fa fa-facebook"></i><span>Facebook</span></a>
                        <a href="#"><i class="fa fa-twitter"></i><span>Twitter</span></a>
                        <a href="#"><i class="fa fa-google-plus"></i><span>Google +</span></a>
                        <a href="#"><i class="fa fa-linkedin"></i><span>linkedin</span></a>
                        <a href="#"><i class="fa fa-instagram"></i><span>Instagram</span></a>
                        <a href="#"><i class="fa fa-vimeo"></i><span>Vimeo</span></a>
                        <a href="#"><i class="fa fa-youtube"></i><span>Youtube</span></a>
                    </div>
                    <!-- Footer Curve Line -->
                    <img class="footer-curve" src=" {{ asset('img/core-img/foo-curve.png') }}" alt="">
                </div>
            </div> --}}

            <div class="row">
                {{-- <div class="col-12">
                    <button class="btn btn-primary cc" id="cc">copyright</button>
                </div> --}}
                <div class="col-12">
                    <div class="copywrite-text text-center" id="ok">
                        <p style="bottom: 0; left: 1;"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area Start ##### -->

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="{{ asset('js/jquery/jquery-2.2.4.min.js') }}"></script>
    <!-- Popper js -->
    <script src="{{ asset('js/bootstrap/popper.min.js') }}"></script>
    <!-- Bootstrap js -->
    <script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
    <!-- All Plugins js -->
    <script src="{{ asset('js/plugins/plugins.js') }}"></script>
    <!-- Active js -->
    <script src="{{ asset('js/active.js') }}"></script>

    @yield('script')


    <script>
        var msg = '{{Session::get('alert')}}';
        var exist = '{{Session::has('alert')}}';
        if(exist){
            alert(msg);
        }
    </script>

    @stack('script')

    
</body>

</html>