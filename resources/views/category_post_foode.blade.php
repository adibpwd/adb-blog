@extends('layouts.foode')

@section('title', 'category Post')

@section('head')
  <link rel="stylesheet" href="{{ asset('css/style2.css') }}">   
@endsection

@section('content')

    @if (count($listCategory) > 0)
    <div class="container mt-5 text-center">
        @for ($i = 0; $i < count($listCategory); $i++)
                @php
                    $userPost = listPost($listCategory[$i]['post_id']);  
                @endphp
                {{-- {{ dd($userPost) }}  --}}
                {{-- <div class="d-flex justify-content-center"> --}}
                    <div class="cardPost mt-3 mb-3 ml-1">
                        <a href="/profil/{{ $userPost['users']['username'] }}">
                            <div class="img-avatar">
                                <img src="https://res.cloudinary.com/duh6epdw5/image/upload/{{ $userPost['users']['foto_profil']}}" alt="" class="rounded-circle" style="width: 100%; height: 100%;">
                            </div>
                        </a>
                        <div class="cardPost-text">
                            <div class="portada" style="background-image: url('https://res.cloudinary.com/duh6epdw5/image/upload/{{ $userPost['thumbnail'] }}');">
                            </div>
                            <a href="/single_post/{{ $userPost['slug'] }}">
                                <div class="title-total text-left">
                                    <div class="title">{{ $userPost['users']['username'] }}</div>
                                    <h6 class="ml-1">{{ Str::limit($userPost['title'], 70) }}</h6>
                    
                                    <div class="desc">{{ strip_tags(Str::limit($userPost['content'], 395)) }}</div>
                                    {{-- <div class="actions text-right">
                                        <a href="/single_post/{{ $userPost['title'] }}"><button><i class="fas fa-eye"></i></button></a>
                                    </div> --}}
                                </div>
                            </a>
                        </div>
                    </div>
                {{-- </div> --}}
        @endfor
    </div>
        <div class="d-flex justify-content-center mt-5 mb-5">
            {{ $listCategory->links() }}
        </div>
    @else
        <h1 class="text-center mt-5">tidak ada article di kategori ini</h1>
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    @endif



@endsection
