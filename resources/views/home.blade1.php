@extends('layouts.main')

@section('title', 'Home')

@section('head')
    <link href='https://fonts.googleapis.com/css?family=Black Ops One' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Ceviche One' rel='stylesheet'>
@endsection

@section('content')

<!-- <div class="header"> 
    <div class="splitview skewed rounded">
        <div class="panel bottom">
            <div class="content">
                <div class="description">
                    <h1>The original image.</h1>
                    <p>This is how the image looks like before applying a duotone effect.</p>
                </div>

                {{-- <img src="{{ asset('img/game-of-thrones.jpg') }}" alt="profil" class="rounded-circle image-footer"
                    style="filter: saturate(8); opacity: 0.5;"> --}}
            </div>
        </div>

        <div class="panel top">
            <div class="content">
                <div style="text-align: center;" class="mt-4">
                    <p style="font-family: 'Black Ops One'; font-size: 58px; margin: -18px 0;"><b>BEST ARTICLE</b></p>
                    <p style="font-family: 'Ceviche One'; font-size: 25px; ">4 Best Article From 4 Category</p>
                </div>
                 <div class="row" style="margin: 0 0;">
                    <div class="col-md-3">
                        <div class='walkthrough show reveal'>
                            <div class='walkthrough-pagination'>
                                <a class='dot active'></a>
                                <a class='dot'></a>
                                <a class='dot'></a>
                                <a class='dot'></a>
                                <a class='dot'></a>
                            </div>
                            <div class='walkthrough-body'>
                                <ul class='screens animate'>
                                    <li class='screen active'>
                                        <div class='media logo'>
                                            <img class='logo' src='https://s3.amazonaws.com/jebbles-codepen/icon.png'>
                                        </div>
                                        <h3>
                                            Product Intro
                                            <br>Walkthrough</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen'>
                                        <div class='media books'>
                                            <img class='icon' alt="icon2nan" src='https://s3.amazonaws.com/jebbles-codepen/book_icon_1.png'>
                                            <img class='icon' alt="icon2nan" src='https://s3.amazonaws.com/jebbles-codepen/book_icon_2.png'>
                                            <img class='icon' alt="icon2nan" src='https://s3.amazonaws.com/jebbles-codepen/book_icon_3.png'>
                                        </div>
                                        <h3>
                                            Data and File
                                            <br>Management</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen'>
                                        <div class='media bars'>
                                            <img class='icon' alt="icon2nan" src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_axis.png'>
                                            <img class='icon' alt="icon2nan" src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_3.png'>
                                            <img class='icon' alt="icon2nan" src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_2.png'>
                                            <img class='icon' alt="icon2nan" src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_1.png'>
                                        </div>
                                        <h3>
                                            Analytics
                                            <br>and Metrics</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen'>
                                        <div class='media files'>
                                            <img class='icon' alt="icon2nan" src='https://s3.amazonaws.com/jebbles-codepen/file_icon_1.png'>
                                            <img class='icon' alt="icon2nan" src='https://s3.amazonaws.com/jebbles-codepen/file_icon_2.png'>
                                            <img class='icon' alt="icon2nan" src='https://s3.amazonaws.com/jebbles-codepen/file_icon_3.png'>
                                            <img class='icon' alt="icon2nan" src='https://s3.amazonaws.com/jebbles-codepen/file_icon_4.png'>
                                        </div>
                                        <h3>
                                            Reporting
                                            <br>and Insights</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen'>
                                        <div class='media comm'>
                                            <img class='icon' alt="icon2nan" src='https://s3.amazonaws.com/jebbles-codepen/comm_icon_1.png'>
                                            <img class='icon' alt="icon2nan" src='https://s3.amazonaws.com/jebbles-codepen/comm_icon_2.png'>
                                        </div>
                                        <h3>
                                            Communications
                                            <br>Tools</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                </ul>
                                <button class='prev-screen'>
                                    <i class="fas fa-angle-left"></i>
                                </button>
                                <button class='next-screen'>
                                    <i class="fas fa-angle-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3" style="z-index: 10;">
                        <div class='walkthrough_1 show_1 reveal_1'>
                            <div class='walkthrough-pagination_1'>
                                <a class='dot_1 active_1'></a>
                                <a class='dot_1'></a>
                                <a class='dot_1'></a>
                                <a class='dot_1'></a>
                                <a class='dot_1'></a>
                            </div>
                            <div class='walkthrough-body_1'>
                                <ul class='screens_1 animate_1'>
                                    <li class='screen_1 active_1'>
                                        <div class='media_1 logo_1'>
                                            <img class='logo_1' src='https://s3.amazonaws.com/jebbles-codepen/icon.png'>
                                        </div>
                                        <h3>
                                            Product Intro
                                            <br>Walkthrough</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen_1'>
                                        <div class='media_1 books_1'>
                                            <img class='icon_1' alt="icon2nan" src='https://s3.amazonaws.com/jebbles-codepen/book_icon_1.png'>
                                            <img class='icon_1' alt="icon2nan" src='https://s3.amazonaws.com/jebbles-codepen/book_icon_2.png'>
                                            <img class='icon_1' alt="icon2nan" src='https://s3.amazonaws.com/jebbles-codepen/book_icon_3.png'>
                                        </div>
                                        <h3>
                                            Data and File
                                            <br>Management</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen_1'>
                                        <div class='media_1 bars_1'>
                                            <img class='icon_1' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_axis.png'>
                                            <img class='icon_1' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_3.png'>
                                            <img class='icon_1' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_2.png'>
                                            <img class='icon_1' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_1.png'>
                                        </div>
                                        <h3>
                                            Analytics
                                            <br>and Metrics</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen_1'>
                                        <div class='media_1 files_1'>
                                            <img class='icon_1' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_1.png'>
                                            <img class='icon_1' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_2.png'>
                                            <img class='icon_1' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_3.png'>
                                            <img class='icon_1' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_4.png'>
                                        </div>
                                        <h3>
                                            Reporting
                                            <br>and Insights</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen_1'>
                                        <div class='media_1 comm_1'>
                                            <img class='icon_1' src='https://s3.amazonaws.com/jebbles-codepen/comm_icon_1.png'>
                                            <img class='icon_1' src='https://s3.amazonaws.com/jebbles-codepen/comm_icon_2.png'>
                                        </div>
                                        <h3>
                                            Comm_1unications
                                            <br>Tools</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                </ul>
                                <button class='prev-screen_1'>
                                    <i class="fas fa-angle-left"></i>
                                </button>
                                <button class='next-screen_1'>
                                    <i class="fas fa-angle-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3" style="z-index: 10;">
                        <div class='walkthrough_2 show_2 reveal_2'>
                            <div class='walkthrough-pagination_2'>
                                <a class='dot_2 active_2'></a>
                                <a class='dot_2'></a>
                                <a class='dot_2'></a>
                                <a class='dot_2'></a>
                                <a class='dot_2'></a>
                            </div>
                            <div class='walkthrough-body_2'>
                                <ul class='screens_2 animate_2'>
                                    <li class='screen_2 active_2'>
                                        <div class='media_2 logo_2'>
                                            <img class='logo_2' src='https://s3.amazonaws.com/jebbles-codepen/icon.png'>
                                        </div>
                                        <h3>
                                            Product Intro
                                            <br>Walkthrough</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen_2'>
                                        <div class='media_2 books_2'>
                                            <img class='icon_2' alt="icon2nan" src='https://s3.amazonaws.com/jebbles-codepen/book_icon_2.png'>
                                            <img class='icon_2' alt="icon2nan" src='https://s3.amazonaws.com/jebbles-codepen/book_icon_2.png'>
                                            <img class='icon_2' alt="icon2nan" src='https://s3.amazonaws.com/jebbles-codepen/book_icon_3.png'>
                                        </div>
                                        <h3>
                                            Data and File
                                            <br>Management</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen_2'>
                                        <div class='media_2 bars_2'>
                                            <img class='icon_2' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_axis.png'>
                                            <img class='icon_2' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_3.png'>
                                            <img class='icon_2' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_2.png'>
                                            <img class='icon_2' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_2.png'>
                                        </div>
                                        <h3>
                                            Analytics
                                            <br>and Metrics</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen_2'>
                                        <div class='media_2 files_2'>
                                            <img class='icon_2' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_2.png'>
                                            <img class='icon_2' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_2.png'>
                                            <img class='icon_2' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_3.png'>
                                            <img class='icon_2' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_4.png'>
                                        </div>
                                        <h3>
                                            Reporting
                                            <br>and Insights</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen_2'>
                                        <div class='media_2 comm_2'>
                                            <img class='icon_2' src='https://s3.amazonaws.com/jebbles-codepen/comm_icon_2.png'>
                                            <img class='icon_2' src='https://s3.amazonaws.com/jebbles-codepen/comm_icon_2.png'>
                                        </div>
                                        <h3>
                                            Comm_2unications
                                            <br>Tools</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                </ul>
                                <button class='prev-screen_2'>
                                    <i class="fas fa-angle-left"></i>
                                </button>
                                <button class='next-screen_2'>
                                    <i class="fas fa-angle-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3" style="z-index: 10;">
                        <div class='walkthrough_3 show_3 reveal_3'>
                            <div class='walkthrough-pagination_3'>
                                <a class='dot_3 active_3'></a>
                                <a class='dot_3'></a>
                                <a class='dot_3'></a>
                                <a class='dot_3'></a>
                                <a class='dot_3'></a>
                            </div>
                            <div class='walkthrough-body_3'>
                                <ul class='screens_3 animate_3'>
                                    <li class='screen_3 active_3'>
                                        <div class='media_3 logo_3'>
                                            <img class='logo_3' src='https://s3.amazonaws.com/jebbles-codepen/icon.png'>
                                        </div>
                                        <h3>
                                            Product Intro
                                            <br>Walkthrough</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen_3'>
                                        <div class='media_3 books_3'>
                                            <img class='icon_3' alt="icon2nan" src='https://s3.amazonaws.com/jebbles-codepen/book_icon_3.png'>
                                            <img class='icon_3' alt="icon2nan" src='https://s3.amazonaws.com/jebbles-codepen/book_icon_3.png'>
                                            <img class='icon_3' alt="icon2nan" src='https://s3.amazonaws.com/jebbles-codepen/book_icon_3.png'>
                                        </div>
                                        <h3>
                                            Data and File
                                            <br>Management</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen_3'>
                                        <div class='media_3 bars_3'>
                                            <img class='icon_3' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_axis.png'>
                                            <img class='icon_3' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_3.png'>
                                            <img class='icon_3' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_3.png'>
                                            <img class='icon_3' src='https://s3.amazonaws.com/jebbles-codepen/bar_icon_3.png'>
                                        </div>
                                        <h3>
                                            Analytics
                                            <br>and Metrics</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen_3'>
                                        <div class='media_3 files_3'>
                                            <img class='icon_3' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_3.png'>
                                            <img class='icon_3' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_3.png'>
                                            <img class='icon_3' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_3.png'>
                                            <img class='icon_3' src='https://s3.amazonaws.com/jebbles-codepen/file_icon_4.png'>
                                        </div>
                                        <h3>
                                            Reporting
                                            <br>and Insights</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                    <li class='screen_3'>
                                        <div class='media_3 comm_3'>
                                            <img class='icon_3' src='https://s3.amazonaws.com/jebbles-codepen/comm_icon_1.png'>
                                            <img class='icon_3' src='https://s3.amazonaws.com/jebbles-codepen/comm_icon_1.png'>
                                        </div>
                                        <h3>
                                            Comm_3unications
                                            <br>Tools</br>
                                        </h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris.</p>
                                    </li>
                                </ul>
                                <button class='prev-screen_3'>
                                    <i class="fas fa-angle-left"></i>
                                </button>
                                <button class='next-screen_3'>
                                    <i class="fas fa-angle-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <img src="{{ asset('img/game-of-thrones.jpg') }}" alt="profil" class="rounded-circle image-footer"
                    style="filter: saturate(1); opacity: 0.2; z-index: 0;">
            </div>
        </div>

        <div class="handle"></div>
    </div>
</div> -->

<textarea class="form-control" id="summary-ckeditor" name="summary-ckeditor"></textarea>




@endsection

{{-- <!-- @section('script')

@endsection --> --}}