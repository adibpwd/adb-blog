<?php

namespace App\Http\Controllers;

// public Intervention\Image\Image filesize()
use Validator;
use DB;
use Response;
// use \Request;
use Illuminate\Http\Request;
use JD\Cloudder\Facades\Cloudder;
use Illuminate\Support\Facades\Auth;
// use Intervention\Image\ImageManagerStatic as Image;
use Intervention\Image\Facades\Image;
use App\Post;
use App\Comment;
// use App\Image;
use App\User;
use App\Category;
use App\Category_Post;
use App\Categories_Heroku;
use App\Liker;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;
// OR
use Artesaos\SEOTools\Facades\SEOTools;
 
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($username)
    {
        // dd($username . Auth::user()->username);
        // $allPost = Post::all();
        // return view('dashboard-article', compact('allPost'));
        if($username == Auth::user()->username) {
            $user = User::where('username', $username)->first();
            $post = Post::where('author_id', $user['id'])->paginate(25);
            // dd($post);
            return view('dashboard-article', compact('user', 'post'));
        } else {
            return 'forbidden to open dashboard ' . $username;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd();

      

        request()->validate([
            'title' => 'unique:posts|required|min:50|max:150|string',
            'content' => 'required|min:300',
            'thumbnail' => 'required|max:1000',
        ],
        [
            'title.required' => 'judul penting banget jangan lupa di isi!',
            'title.min' => 'pendek amat judulnya..tambah lagi y! min 50 huruf',
            'title.max' => 'judul kepanjangan..jangan melebihi 150 huruf',
            'title.string' => 'judul bukan string',
            'title.unique' => 'judul udah ada',
            'content.min' => 'konten terlalu pendek min 300 huruf..tambah lagi sampai ribuah huruf',
            'content.required' => 'apa yang mau dibaca kalau konten nya kosong',
            'thumbnail.max' => 'file terlalu besar kecil kan dulu size maximal 1mb terus upload lagi..terimakasih',
            'thumbnail.required' => 'thumbnail belum kamu upload..kasih thumbnail supaya banyak orang tertarik dengan artikelmu',
        ]);
          
            // dd($request);
            
        

        
        $file = $request->file('thumbnail');
        // dd($file);
        $mimeFile = $file->getMimeType();
        $mimeFile = substr($mimeFile, 6);
        if($mimeFile == 'jpeg') {
            $mimeFile = 'jpg';
        }

        $sizeFile = $file->getSize();
        // dd($sizeFile);
        // if($sizeFile > 1001) {
        //     return redirect()->back();
        // }
        // $path = $file->getRealPath();
        // dd($file);
                // create an image
        // $file = Image::make($file);
        // $file->resize(300, null, function ($constraint) {
        //     $constraint->aspectRatio();
        // });

        // get file size
        // $size = $img->width();        
        // dd($file);
       

        // dd($request);
        // $file = $request->file('thumbnail');
        // dd($mimeFile);   
        $upload = \Cloudder::upload($file);
        $result = Cloudder::getResult();
        $nameFile = 'v' . $result['version'] . '/' .Cloudder::getPublicId() . '.' . $mimeFile;
        // dd();
        if($request['tags']) {
            $request->request->add(['tags' => implode(',', $request['tags'])]);
        }
        // dd($request);
        // $request->tags = implode(',', $request['tags']);
        $post = Post::create($request->except('categories'));
        $post->thumbnail = $nameFile;
        $post->author_id = Auth::user()->id;
        $post->slug = str_slug($request['title']);
        $post->save();

        // dd($post->id); 
        // selain heroku
        for ($i=0; $i < count($request->categories); $i++) { 
            Category_Post::create(['post_id' => $post->id, 'category_id' => $request->categories[$i]]);
        }
            

        // buat diheroku
        // $categories = implode(',', $request['categories']);
        //   Categories_Heroku::create(['post_id' => $post->id, 'category_name' => $categories]);
        

        

        return redirect('dashboard/'.Auth::user()->username);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request['tags']);

        request()->validate([
            // 'title' => ['required', 'min:50', 'max:100','string'],
            'title' => 'required|min:50|max:150|string',
            'content' => 'required|min:300',
            'thumbnail' => 'max:1000',
        ],
        [
            // 'required' => ':attribute belum diisi',
            'title.required' => 'judul penting banget jangan lupa di isi!',
            'title.min' => 'pendek amat judulnya..tambah lagi y! min 50 huruf',
            'title.max' => 'judul kepanjangan..jangan melebihi 150 huruf',
            'title.string' => 'judul bukan string',
            // 'title.unique' => 'judul udah ada',
            'content.min' => 'konten terlalu pendek min 300 huruf..tambah lagi sampai ribuah huruf',
            'content.required' => 'apa yang mau dibaca kalau konten nya kosong',
            'thumbnail.max' => 'file terlalu besar kecil kan dulu size maximal 1mb terus upload lagi..terimakasih',
        ]);
        // dd($request['id']);
        
        // $request->request->add(['tags' => implode(',', $request['tags'])]);
        if($request['tags']) {
            $tags = implode(',', $request['tags']);
        } else {
            $tags = 'tag kosong';
        }

        // dd($tags);
        // $request->save();

        $file = $request->file('thumbnail');

        if($file) {
            $mimeFile = $file->getMimeType();
            $upload = \Cloudder::upload($file);
            $result = Cloudder::getResult();
            $nameFile = 'v' . $result['version'] . '/' .Cloudder::getPublicId() . '.' . substr($mimeFile, 6);
            $post = Post::where('id', $request['id'])->update(
                [
                'title' => $request['title'],
                'content' => $request['content'],
                'thumbnail' => $nameFile,
                'tags' => $tags,
                'slug' => str_slug($request['title'])
                ]);
        } else {
            $post = Post::where('id', $request['id'])->update(['title' => $request['title'], 'content' => $request['content'], 'tags' => $tags, 'slug' => str_slug($request['title'])]);
        }

        // dd($post);
        // $post->tags = $request->request->add(['tags' => implode(',', $request['tags'])]);

        Category_Post::where('post_id', $request['id'])->delete();
        if( $request->categories ) {
            for ($i=0; $i < count($request->categories); $i++) { 
                Category_Post::create(['post_id' => $request['id'], 'category_id' => $request['categories'][$i]]);
            }
        }
        
        return redirect('dashboard/'.Auth::user()->username);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // jika pakai mysql db:statement() dipake jika postgress nggak
        // mysql
        // dd($post_user_id);
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        $post_user = Post::where('author_id', Auth::user()->id)->where('id', $id)->delete();
        if($post_user) {
            Category_Post::where('post_id', $id)->delete();
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
            return redirect()->back();
        } else {
            // return redirect()->back()->with('alert', 'post tidak ditemukan');
            return redirect()->back()->with('success', 'your message,here');   
        }
        // Post::where('id', $id)->delete();
    }

    public function upload(Request $request)
    {
        
        // $file = $request->file('upload');
        // dd($file);
        // $nameFile = $file->getClientOriginalName();
        // $pathFile = $file->getRealPath();
        // $sizeFile = $file->getSize();




        if ($request->hasFile('upload')) {
            $file = $request->file('upload'); //SIMPAN SEMENTARA FILENYA KE VARIABLE
            $fileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME); //KITA GET ORIGINAL NAME-NYA
            // dd($fileName);
            //KEMUDIAN GENERATE NAMA YANG BARU KOMBINASI NAMA FILE + TIME
            $fileName = $fileName . '_' . time() . '.' . $file->getClientOriginalExtension();
            $mimeFile = $file->getMimeType();
            // dd();
            // $file->move(public_path('uploads'), $fileName); //SIMPAN KE DALAM FOLDER PUBLIC/UPLOADS
            \Cloudder::upload($file);
            // dd(Cloudder::getResult());
            //KEMUDIAN KITA BUAT RESPONSE KE CKEDITOR
            $ckeditor = $request->input('CKEditorFuncNum');

            $url = 'https://res.cloudinary.com/duh6epdw5/image/upload/v1590659238/' . Cloudder::getPublicId() . '.' . substr($mimeFile, 6);
            // dd($url); 
            $msg = 'Image uploaded successfully'; 
            //DENGNA MENGIRIMKAN INFORMASI URL FILE DAN MESSAGE
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($ckeditor, '$url', '$msg')</script>";
    
            //SET HEADERNYA
            @header('Content-type: text/html; charset=utf-8'); 
            return $response;
        }
        
    }

    public function onePost($slug) {
        // dd($title);
        // username belum bisa manggil username doang
        $post = Post::where('slug', $slug)->with(['users:id,username', 'categoriesName'])->first();
        // dd();
        // SEOMeta::setTitle($post['title']);
        SEOMeta::setDescription('lathisiro.herokuapp.com');
        SEOMeta::setCanonical('lathisiro.herokuapp.com');
        // dd($post);
        // if($post['tags']) {
            $tags = explode(',', $post['tags']);
        // } else {
        //     $tags = null;  
        // }
        // add aja
        // if ($post) {
            $comments = Comment::where('post_id', $post['id'])->orderBy('created_at', 'desc')->paginate(5);
            $postUser = Post::where('author_id', $post['users']['id'])->limit(5)->get();
            $likers_count = Liker::where('post_id', $post['id'])->count();
            // dd($tags);
            
            if(Auth::user()) {
                $user_like = Liker::where('post_id', $post['id'])->where('user_id', Auth::user()->id)->first();
            } else {
                $user_like = null;
            }

            if($user_like != null) {
                $check = 'checked';
            } else {
                $check = '';
            }
            // dd($post);
            if(Auth::user()) {
                $userLogged = Auth::user()->only(['id', 'username']);
                return view('single_post_foode', compact('post', 'postUser', 'userLogged', 'comments', 'tags', 'likers_count', 'check', 'user_like'));
            } else {
                return view('single_post_foode', compact('post', 'postUser', 'comments', 'tags', 'likers_count', 'check', 'user_like'));
            }
            // dd($postUser);
        // }


    }

    public function editor($editor, $idPost) {
        $post = Post::where('id', $idPost)->first();
        

        if($post['author_id'] ==  Auth::user()->id) {
            return view('editor_foode', compact('post', 'editor'));
        } else {
            return "<h1>Post Tidak Di Temukan</h1>";
        }

        // if($idPost == NULL) {
        //     return 'hello world';
        // }
        // // $post = 
    }

    public function newPost() {
        $editor = 'new post';
        // buat selain diheroku
        $allCategory = Category::all();
        
        // dd($allCategory);
        return view('editor_foode', compact('editor', 'allCategory'));
    }

    public function editPost($idPost) {
        $editor = 'edit post';
        // selain heroku
        $post = Post::where('id', $idPost)->with(['categoriesName'])->first();
        
        $tags = explode(',', $post['tags']);
        $allCategory = Category::all();


        // selain heroku
        if($post['author_id'] ==  Auth::user()->id) {
            return view('editor_foode', compact('post', 'editor', 'allCategory', 'tags'));
        } else {
            return "<h1>Post Tidak Di Temukan</h1>";
        }
    }

    public function like(Request $request) {
        // return dd($request['user_id']);
        Liker::create([
            'user_id' => $request['user_id'],
            'post_id' => $request['post_id']
        ]);
        $request_count = Liker::where('post_id', $request['post_id'])->count();
        return $request_count;
    }

    public function unlike(Request $request) {
        // return dd($request['user_id']);
        Liker::where('post_id', $request['post_id'])->where('user_id', $request['user_id'])->delete();
        $request_count = Liker::where('post_id', $request['post_id'])   ->count();
        return $request_count;
    }
}
 
