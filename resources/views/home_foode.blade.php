@extends('layouts.foode')

@section('head')
    <link href='https://fonts.googleapis.com/css?family=Metal Mania' rel='stylesheet'>
@endsection

@section('author', 'home')

@section('content')



    <!-- ##### Hero Area Start ##### -->
    <section class="hero-area">
        <div class="hero-post-slides owl-carousel">

            @for ($i = 0; $i < 7; $i++)
                <!-- Single Hero Post -->
                <div class="single-hero-post rounded">
                    <!-- Post Image -->
                    <div class="slide-img bg-img" style="background-image: url(https://res.cloudinary.com/duh6epdw5/image/upload/{{ $newPost[$i]['thumbnail'] }})"></div>
                    <!-- Post Content -->
                    <div class="hero-slides-content">
                        <p>{{ date('d M Y | D', strtotime($newPost[$i]['updated_at'])) }} <br> {{ count($newPost[$i]['comments']) }} Comments</p>
                        <a href="/single_post/{{ $newPost[$i]['slug'] }}" class="post-title">
                            <h4>{{ Str::limit($newPost[$i]['title'], 30) }}</h4>
                        </a>
                    </div>
                </div>
            @endfor
        </div>
    </section> 
    <!-- ##### Hero Area End ##### -->

    <!-- ##### Blog Content Area Start ##### -->
    <section class="blog-content-area section-padding-100">
        <div class="container">
            <div class="row">
                <!-- Featured Post Area -->
                <div class="col-12">
                    <div class="featured-post-area">
                        <div id="featured-post-slides" class="carousel slide d-flex flex-wrap" data-ride="carousel">

                            <div class="carousel-inner">

                                @for ($i = 7; $i < 12; $i++)
                                    @if ($i == 7)
                                        <div class="carousel-item active bg-img"
                                            style="background-image: url(https://res.cloudinary.com/duh6epdw5/image/upload/{{ $newPost[$i]['thumbnail'] }})">
                                            <!-- Featured Post Content -->
                                            <div class="featured-post-content">
                                                <p>{{ date('d M Y | D', strtotime($newPost[$i]['updated_at'])) }}</p>
                                                <a href="/single_post/{{ $newPost[$i]['slug'] }}" class="post-title">
                                                    <h2>{{ Str::limit($newPost[$i]['title'], 60) }}</h2>
                                                </a>
                                            </div>
                                        </div>
                                    @else
                                        <div class="carousel-item bg-img"
                                            style="background-image: url(https://res.cloudinary.com/duh6epdw5/image/upload/{{ $newPost[$i]['thumbnail'] }})">
                                            <!-- Featured Post Content -->
                                            <div class="featured-post-content">
                                                <p>{{ date('d M Y | D', strtotime($newPost[$i]['updated_at'])) }}</p>
                                                <a href="/single_post/{{ $newPost[$i]['slug'] }}" class="post-title">
                                                    <h2>{{ Str::limit($newPost[$i]['title'], 60) }}</h2>
                                                </a>
                                            </div>
                                        </div>    
                                    @endif
                                @endfor
                            </div>

                            <ol class="carousel-indicators">
                                @for ($i = 7; $i < 12; $i++)
                                    @if ($i == 7)
                                        <li data-target="#featured-post-slides" data-slide-to="0" class="active">
                                            <h2>01</h2>
                                            <a href="#" class="post-title">
                                                <h5>{{ Str::limit($newPost[$i]['title'], 30) }}</h5>
                                            </a>
                                        </li>
                                    @else
                                        <li data-target="#featured-post-slides" data-slide-to="{{ $i-7 }}">
                                            <h2>0{{ $i-6 }}</h2>
                                            <a href="#" class="post-title">
                                                <h5>{{ Str::limit($newPost[$i]['title'], 30) }}</h5>
                                            </a>
                                        </li>
                                    @endif
                                @endfor
                            </ol>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">
                <!-- Blog Posts Area -->
                <div class="col-12 col-lg-8">
                    {{-- <div class="blog-posts-area">

                        <!-- Single Blog Post -->
                        <div class="single-blog-post d-flex flex-wrap mt-50">
                            <!-- Thumbnail -->
                            <div class="post-thumbnail mb-50">
                                <a href="#">
                                <img src="https://res.cloudinary.com/duh6epdw5/image/upload/v1590496154/{{ $newPost[0]['thumbnail'] }}" alt="gagal muat">
                                </a>
                            </div>
                            <!-- Content -->
                            <div class="post-content mb-50">
                                <p class="post-date">MAY 8, 2018 / foody</p>
                                <a href="#" class="post-title">
                                    <h4>Vegetarian Spring Pho with Sweet Potato Noodles and Heirloom Beans</h4>
                                </a>
                                <div class="post-meta">
                                    <a href="#"><span>by</span> Sarah Jenks</a>
                                    <a href="#"><i class="fa fa-eye"></i> 192</a>
                                    <a href="#"><i class="fa fa-comments"></i> 08</a>
                                </div>
                                <p class="post-excerpt">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit.</p>
                                <a href="#" class="read-more-btn">Continue Reading <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                            </div>
                            <!-- Post Curve Line -->
                            <img class="post-curve-line" src=" {{ asset('img/core-img/post-curve-line.png') }}" alt="">
                        </div>

                        <!-- Single Blog Post -->
                        <div class="single-blog-post d-flex flex-wrap mt-50">
                            <!-- Thumbnail -->
                            <div class="post-thumbnail mb-50">
                                <a href="#"><img src=" {{ asset('img/blog-img/9.jpg') }}" alt=""></a>
                            </div>
                            <!-- Content -->
                            <div class="post-content mb-50">
                                <p class="post-date">MAY 12, 2018 / drinks</p>
                                <a href="#" class="post-title">
                                    <h4>Grain-Free Sweet &amp; Savory Activated Walnut Granola</h4>
                                </a>
                                <div class="post-meta">
                                    <a href="#"><span>by</span> Sarah Jenks</a>
                                    <a href="#"><i class="fa fa-eye"></i> 192</a>
                                    <a href="#"><i class="fa fa-comments"></i> 08</a>
                                </div>
                                <p class="post-excerpt">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit.</p>
                                <a href="#" class="read-more-btn">Continue Reading <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                            </div>
                            <!-- Post Curve Line -->
                            <img class="post-curve-line" src=" {{ asset('img/core-img/post-curve-line.png') }}" alt="">
                        </div>

                        <!-- Single Blog Post -->
                        <div class="single-blog-post d-flex flex-wrap mt-50">
                            <!-- Thumbnail -->
                            <div class="post-thumbnail mb-50">
                                <a href="#"><img src=" {{ asset('img/blog-img/10.jpg') }}" alt=""></a>
                            </div>
                            <!-- Content -->
                            <div class="post-content mb-50">
                                <p class="post-date">MAY 15, 2018 / Coffee</p>
                                <a href="#" class="post-title">
                                    <h4>Self-Care Interview Series: Gabrielle Russomagno</h4>
                                </a>
                                <div class="post-meta">
                                    <a href="#"><span>by</span> Sarah Jenks</a>
                                    <a href="#"><i class="fa fa-eye"></i> 192</a>
                                    <a href="#"><i class="fa fa-comments"></i> 08</a>
                                </div>
                                <p class="post-excerpt">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit.</p>
                                <a href="#" class="read-more-btn">Continue Reading <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                            </div>
                            <!-- Post Curve Line -->
                            <img class="post-curve-line" src=" {{ asset('img/core-img/post-curve-line.png') }}" alt="">
                        </div>

                        <!-- Single Blog Post -->
                        <div class="single-blog-post d-flex flex-wrap mt-50">
                            <!-- Thumbnail -->
                            <div class="post-thumbnail mb-50">
                                <a href="#"><img src=" {{ asset('img/blog-img/11.jpg') }}" alt=""></a>
                            </div>
                            <!-- Content -->
                            <div class="post-content mb-50">
                                <p class="post-date">MAY 20, 2018 / foody</p>
                                <a href="#" class="post-title">
                                    <h4>Green Skillet Pizza with Asparagus and Pesto</h4>
                                </a>
                                <div class="post-meta">
                                    <a href="#"><span>by</span> Sarah Jenks</a>
                                    <a href="#"><i class="fa fa-eye"></i> 192</a>
                                    <a href="#"><i class="fa fa-comments"></i> 08</a>
                                </div>
                                <p class="post-excerpt">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit.</p>
                                <a href="#" class="read-more-btn">Continue Reading <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                            </div>
                            <!-- Post Curve Line -->
                            <img class="post-curve-line" src=" {{ asset('img/core-img/post-curve-line.png') }}" alt="">
                        </div>

                        <!-- Single Blog Post -->
                        <div class="single-blog-post d-flex flex-wrap mt-50">
                            <!-- Thumbnail -->
                            <div class="post-thumbnail mb-50">
                                <a href="#"><img src=" {{ asset('img/blog-img/12.jpg') }}" alt=""></a>
                            </div>
                            <!-- Content -->
                            <div class="post-content mb-50">
                                <p class="post-date">MAY 28, 2018 / health</p>
                                <a href="#" class="post-title">
                                    <h4>Green Skillet Pizza with Asparagus and Pesto</h4>
                                </a>
                                <div class="post-meta">
                                    <a href="#"><span>by</span> Sarah Jenks</a>
                                    <a href="#"><i class="fa fa-eye"></i> 192</a>
                                    <a href="#"><i class="fa fa-comments"></i> 08</a>
                                </div>
                                <p class="post-excerpt">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit.</p>
                                <a href="#" class="read-more-btn">Continue Reading <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                            </div>
                            <!-- Post Curve Line -->
                            <img class="post-curve-line" src=" {{ asset('img/core-img/post-curve-line.png') }}" alt="">
                        </div>

                    </div>

                    <!-- Pager -->
                    <ol class="foode-pager mt-50">
                        <li><a href="#"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Newer</a></li>
                        <li><a href="#">Older <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></li>
                    </ol> --}}
                </div>

                <!-- Blog Sidebar Area -->
                <div class="col-12 col-sm-9 col-md-6 col-lg-4">
                    <div class="post-sidebar-area">

                        {{-- <!-- ##### Single Widget Area ##### -->
                        <div class="single-widget-area">
                            <!-- Title -->
                            <div class="widget-title">
                                <h6>Latest Posts</h6>
                            </div>

                            <!-- Single Latest Posts -->
                            <div class="single-latest-post d-flex">
                                <div class="post-thumb">
                                    <img src=" {{ asset('img/blog-img/lp1.jpg') }}" alt="">
                                </div>
                                <div class="post-content">
                                    <a href="#" class="post-title">
                                        <h6>Weeknight Ponzu Pasta</h6>
                                    </a>
                                    <a href="#" class="post-author"><span>by</span> Erin Alderson</a>
                                </div>
                            </div>

                            <!-- Single Latest Posts -->
                            <div class="single-latest-post d-flex">
                                <div class="post-thumb">
                                    <img src=" {{ asset('img/blog-img/lp2.jpg') }}" alt="">
                                </div>
                                <div class="post-content">
                                    <a href="#" class="post-title">
                                        <h6>The Most Popular Recipe Last Month</h6>
                                    </a>
                                    <a href="#" class="post-author"><span>by</span> Erin Alderson</a>
                                </div>
                            </div>

                            <!-- Single Latest Posts -->
                            <div class="single-latest-post d-flex">
                                <div class="post-thumb">
                                    <img src=" {{ asset('img/blog-img/lp3.jpg') }}" alt="">
                                </div>
                                <div class="post-content">
                                    <a href="#" class="post-title">
                                        <h6>A Really Good Chana Masala</h6>
                                    </a>
                                    <a href="#" class="post-author"><span>by</span> Erin Alderson</a>
                                </div>
                            </div>

                            <!-- Single Latest Posts -->
                            <div class="single-latest-post d-flex">
                                <div class="post-thumb">
                                    <img src=" {{ asset('img/blog-img/lp4.jpg') }}" alt="">
                                </div>
                                <div class="post-content">
                                    <a href="#" class="post-title">
                                        <h6>Spicy Instant Pot Taco Soup</h6>
                                    </a>
                                    <a href="#" class="post-author"><span>by</span> Erin Alderson</a>
                                </div>
                            </div>

                            <!-- Single Latest Posts -->
                            <div class="single-latest-post d-flex">
                                <div class="post-thumb">
                                    <img src=" {{ asset('img/blog-img/lp5.jpg') }}" alt="">
                                </div>
                                <div class="post-content">
                                    <a href="#" class="post-title">
                                        <h6>Lime Leaf Miso Soup</h6>
                                    </a>
                                    <a href="#" class="post-author"><span>by</span> Erin Alderson</a>
                                </div>
                            </div>

                        </div> --}}

                        <!-- ##### Single Widget Area ##### -->
                        <div class="single-widget-area">
                            <!-- Adds -->
                            <a href="#"><img src=" {{ asset('img/blog-img/add.png') }}" alt=""></a>
                        </div>

                        <!-- ##### Single Widget Area ##### -->
                        <div class="single-widget-area">
                            <!-- Title -->
                            <div class="widget-title">
                                <h6>Newsletter</h6>
                            </div>
                            <!-- Content -->
                            <div class="newsletter-content">
                                <p>Subscribe our newsletter gor get notification about new updates, information discount, etc.</p>
                                <form action="#" method="post">
                                    <input type="email" name="email" class="form-control" placeholder="Your email">
                                    <button><i class="fa fa-send"></i></button>
                                </form>
                            </div>
                        </div>

                        <!-- ##### Single Widget Area ##### -->
                        {{-- <div class="single-widget-area">
                            <!-- Title -->
                            <div class="widget-title">
                                <h6>popular tags</h6>
                            </div>
                            <!-- Tags -->
                            <ol class="popular-tags d-flex flex-wrap">
                                <li><a href="#">Creative</a></li>
                                <li><a href="#">Unique</a></li>
                                <li><a href="#">Template</a></li>
                                <li><a href="#">Photography</a></li>
                                <li><a href="#">travel</a></li>
                                <li><a href="#">lifestyle</a></li>
                                <li><a href="#">Wordpress Template</a></li>
                                <li><a href="#">food</a></li>
                                <li><a href="#">Idea</a></li>
                            </ol>
                        </div> --}}

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Blog Content Area End ##### -->

@endsection