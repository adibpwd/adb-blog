<?php

use Illuminate\Database\Seeder;

class SosmedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sosmed = [
            'adib', 'alpin', 'anonymous'
        ];

        for ($i=0; $i < count($sosmed); $i++) { 
            DB::table('sosmeds')->insert([
                'facebook' => 'https://www.facebook.com/' . $sosmed[$i],
                'twitter' => 'https://twitter.com/alfin' . $sosmed[$i],
                'instagram' => 'https://www.instagram.com/alfin/' . $sosmed[$i],
                'linkedin' => 'https://www.linkedin.com/in/alfin/' . $sosmed[$i],
                'youtube' => 'https://www.youtube.com/user/alfin' . $sosmed[$i]
            ]);
        }

    }
}
