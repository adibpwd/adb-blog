<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    // protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $guarded = ['id', 'status', 'author_id', 'tag_id', 'created_at', 'updated_at'];

    public function users() {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function usersName() {
        return $this->users()->select('username');
    }
    
    public function comments() {
        return $this->hasMany(Comment::class, 'post_id');
    }
    
    public function categoriesName()
    {
        return $this->belongsToMany(Category::class, 'categories_post', 'post_id', 'category_id');
    }

}
 