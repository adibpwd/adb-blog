<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => "email atau kata sandi salah..coba lagi atau klik ' forgot password '",
    'throttle' => 'terlalu banyak upaya masuk..coba lagi nanti y.',

];
