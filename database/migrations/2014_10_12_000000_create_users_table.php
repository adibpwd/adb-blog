<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username');
            $table->string('password');
            $table->string('foto_profil')->default('v1590921012/account_default.jpg');
            $table->string('no_telp')->nullable();
            $table->date('tgl_lahir')->nullable();
            $table->string('foto_header')->default('v1590217083/img-not-found.png');
            $table->unsignedBigInteger('sosmed_id')->nullable();
            $table->foreign('sosmed_id')->references('id')->on('sosmeds');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
