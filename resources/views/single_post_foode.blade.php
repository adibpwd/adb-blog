@extends('layouts.foode')

@section('title', $post['title'])

@section('author', $post['users']['username'])

@push('style')
    <style>
        .resp-sharing-button__link,
        .resp-sharing-button__icon {
        display: inline-block
        }

        .resp-sharing-button__link {
        text-decoration: none;
        color: #fff;
        margin: 0.5em
        }

        .resp-sharing-button {
        border-radius: 5px;
        transition: 25ms ease-out;
        padding: 0.5em 0.75em;
        font-family: Helvetica Neue,Helvetica,Arial,sans-serif
        }

        .resp-sharing-button__icon svg {
        width: 1em;
        height: 1em;
        margin-right: 0.4em;
        vertical-align: top
        }

        .resp-sharing-button--small svg {
        margin: 0;
        vertical-align: middle
        }

        .resp-sharing-button__link .resp-sharing-button svg {
            position: relative!important;
        }

        /* Non solid icons get a stroke */
        .resp-sharing-button__icon {
        stroke: #fff;
        fill: none
        }

        /* Solid icons get a fill */
        .resp-sharing-button__icon--solid,
        .resp-sharing-button__icon--solidcircle {
        fill: #fff;
        stroke: none
        }

        .resp-sharing-button--twitter {
        background-color: #55acee
        }

        .resp-sharing-button--twitter:hover {
        background-color: #2795e9
        }

        .resp-sharing-button--pinterest {
        background-color: #bd081c
        }

        .resp-sharing-button--pinterest:hover {
        background-color: #8c0615
        }

        .resp-sharing-button--facebook {
        background-color: #3b5998
        }

        .resp-sharing-button--facebook:hover {
        background-color: #2d4373
        }

        .resp-sharing-button--tumblr {
        background-color: #35465C
        }

        .resp-sharing-button--tumblr:hover {
        background-color: #222d3c
        }

        .resp-sharing-button--reddit {
        background-color: #5f99cf
        }

        .resp-sharing-button--reddit:hover {
        background-color: #3a80c1
        }

        .resp-sharing-button--google {
        background-color: #dd4b39
        }

        .resp-sharing-button--google:hover {
        background-color: #c23321
        }

        .resp-sharing-button--linkedin {
        background-color: #0077b5
        }

        .resp-sharing-button--linkedin:hover {
        background-color: #046293
        }

        .resp-sharing-button--email {
        background-color: #777
        }

        .resp-sharing-button--email:hover {
        background-color: #5e5e5e
        }

        .resp-sharing-button--xing {
        background-color: #1a7576
        }

        .resp-sharing-button--xing:hover {
        background-color: #114c4c
        }

        .resp-sharing-button--whatsapp {
        background-color: #25D366
        }

        .resp-sharing-button--whatsapp:hover {
        background-color: #1da851
        }

        .resp-sharing-button--hackernews {
        background-color: #FF6600
        }
        .resp-sharing-button--hackernews:hover, .resp-sharing-button--hackernews:focus {   background-color: #FB6200 }

        .resp-sharing-button--vk {
        background-color: #507299
        }

        .resp-sharing-button--vk:hover {
        background-color: #43648c
        }

        .resp-sharing-button--facebook {
        background-color: #3b5998;
        border-color: #3b5998;
        }

        .resp-sharing-button--facebook:hover,
        .resp-sharing-button--facebook:active {
        background-color: #2d4373;
        border-color: #2d4373;
        }

        .resp-sharing-button--twitter {
        background-color: #55acee;
        border-color: #55acee;
        }

        .resp-sharing-button--twitter:hover,
        .resp-sharing-button--twitter:active {
        background-color: #2795e9;
        border-color: #2795e9;
        }

        .resp-sharing-button--pinterest {
        background-color: #bd081c;
        border-color: #bd081c;
        }

        .resp-sharing-button--pinterest:hover,
        .resp-sharing-button--pinterest:active {
        background-color: #8c0615;
        border-color: #8c0615;
        }

        .resp-sharing-button--linkedin {
        background-color: #0077b5;
        border-color: #0077b5;
        }

        .resp-sharing-button--linkedin:hover,
        .resp-sharing-button--linkedin:active {
        background-color: #046293;
        border-color: #046293;
        }

        .resp-sharing-button--reddit {
        background-color: #5f99cf;
        border-color: #5f99cf;
        }

        .resp-sharing-button--reddit:hover,
        .resp-sharing-button--reddit:active {
        background-color: #3a80c1;
        border-color: #3a80c1;
        }

        .resp-sharing-button--whatsapp {
        background-color: #25D366;
        border-color: #25D366;
        }

        .resp-sharing-button--whatsapp:hover,
        .resp-sharing-button--whatsapp:active {
        background-color: #1DA851;
        border-color: #1DA851;
        }

        .resp-sharing-button--hackernews {
        background-color: #FF6600;
        border-color: #FF6600;
        }

        .resp-sharing-button--hackernews:hover
        .resp-sharing-button--hackernews:active {
        background-color: #FB6200;
        border-color: #FB6200;
        }

        .resp-sharing-button--vk {
        background-color: #507299;
        border-color: #507299;
        }

        .resp-sharing-button--vk:hover
        .resp-sharing-button--vk:active {
        background-color: #43648c;
        border-color: #43648c;
        }

        .resp-sharing-button--telegram {
        background-color: #54A9EB;
        }

        .resp-sharing-button--telegram:hover {
        background-color: #4B97D1;}

    </style>
@endpush

@section('head')
    {{-- <link href='https://fonts.googleapis.com/css?family=Metal Mania' rel='stylesheet'> --}}
    <link href='https://fonts.googleapis.com/css?family=Metal Mania' rel='stylesheet'>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    {!! SEOMeta::generate() !!}
@endsection

@section('content') 

@php
    $linkPost = str_replace(" ", "%20", $post['slug']);
@endphp



{{-- @php
    $ok = $post['thumbnail'];
    var_dump("https://res.cloudinary.com/duh6epdw5/image/upload/" . $ok);
    die;
@endphp --}}
{{-- {{ dd($post['thumbnail'] ) }} --}}
{{-- 
<!-- Logo Area -->
<div class="logo-area text-center">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>{{ Auth::user()->username }}</h1>
            </div>
        </div>
    </div>
</div> --}}

<!-- ##### Breadcrumb Area Start ##### -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Blog List</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Blog Single</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ##### Breadcrumb Area End ##### -->

<div class="content-2">
    <div class="" id="main-content1">
        <div style="display: inline-block; text-algin: center;">
            @if (Auth::user())
                <p id="postId" hidden>{{ $post['id'] }}</p>
                <p id="userId" hidden>{{ Auth::user()->id }}</p>
                @endif
                <input type="checkbox" id="checkbox1" {{ $check }}/>
                <label for="checkbox1" id="c1">
                    <span style="position: absolute; top: 30px; left: 40px; z-index: 100;" id="count-like">{{ $likers_count }}</span>
                <svg id="heart-svg" viewBox="467 392 58 57"
                    xmlns="http://www.w3.org/2000/svg">
                    <g id="Group" fill="none" fill-rule="evenodd"
                        transform="translate(467 392)">
                        <path
                            d="M29.144 20.773c-.063-.13-4.227-8.67-11.44-2.59C7.63 28.795 28.94 43.256 29.143 43.394c.204-.138 21.513-14.6 11.44-25.213-7.214-6.08-11.377 2.46-11.44 2.59z"
                            id="heart" fill="#AAB8C2" />
                        <circle id="main-circ" fill="#E2264D" opacity="0" cx="29.5"
                            cy="29.5" r="1.5" />
    
                        <g id="grp7" opacity="0" transform="translate(7 6)">
                            <circle id="oval1" fill="#9CD8C3" cx="2" cy="6" r="2" />
                            <circle id="oval2" fill="#8CE8C3" cx="5" cy="2" r="2" />
                        </g>
    
                        <g id="grp6" opacity="0" transform="translate(0 28)">
                            <circle id="oval1" fill="#CC8EF5" cx="2" cy="7" r="2" />
                            <circle id="oval2" fill="#91D2FA" cx="3" cy="2" r="2" />
                        </g>
    
                        <g id="grp3" opacity="0" transform="translate(52 28)">
                            <circle id="oval2" fill="#9CD8C3" cx="2" cy="7" r="2" />
                            <circle id="oval1" fill="#8CE8C3" cx="4" cy="2" r="2" />
                        </g>
    
                        <g id="grp2" opacity="0" transform="translate(44 6)">
                            <circle id="oval2" fill="#CC8EF5" cx="5" cy="6" r="2" />
                            <circle id="oval1" fill="#CC8EF5" cx="2" cy="2" r="2" />
                        </g>
    
                        <g id="grp5" opacity="0" transform="translate(14 50)">
                            <circle id="oval1" fill="#91D2FA" cx="6" cy="5" r="2" />
                            <circle id="oval2" fill="#91D2FA" cx="2" cy="2" r="2" />
                        </g>
    
                        <g id="grp4" opacity="0" transform="translate(35 50)">
                            <circle id="oval1" fill="#F48EA7" cx="6" cy="5" r="2" />
                            <circle id="oval2" fill="#F48EA7" cx="2" cy="2" r="2" />
                        </g>
    
                        <g id="grp1" opacity="0" transform="translate(24)">
                            <circle id="oval1" fill="#9FC7FA" cx="2.5" cy="3" r="2" />
                            <circle id="oval2" fill="#9FC7FA" cx="7.5" cy="2" r="2" />
                        </g>
                    </g>
                </svg>
            </label>
            <div id="a1" style="display: none;">@if ($check) liked @else like @endif</div>
        </div>
    </div>
</div>

<p id="test" style="display: none;">off</p>
<!-- ##### Blog Content Area Start ##### -->
<section class="blog-content-area">
    <div class="container">
        <div class="row">
            <!-- Blog Posts Area -->
            <div class="col-12 col-lg-8">
                <div class="blog-posts-area">

                    <!-- Post Details Area -->
                    <div class="single-post-details-area">
                        <div class="post-thumbnail mb-30">
                            <img src="https://res.cloudinary.com/duh6epdw5/image/upload/{{ $post['thumbnail'] }}"
                                alt="">
                        </div>
                        <div class="post-content">
                            <p class="post-date">{{ date("Y-m-d / l", strtotime($post['updated_at']))  }}</p>
                            <h4 class="post-title"> {{ $post['title'] }}</h4>
                            <div class="post-meta">
                                <a href="/profil/{{ $post['users']['username'] }}"><span>by</span> {{ $post['users']['username'] }}</a>
                                {{-- <a href="#"><i class="fa fa-eye"></i> 192</a>
                                <a href="#"><i class="fa fa-comments"></i> 08</a> --}}
                                
                            </div>
                            @php
                            echo $post['content']; 
                            @endphp
                        </div>
                    </div>


                    {{-- <div class="container">
                        <div class="effect egeon">
                            <div class="buttons">
                                <a href="http://www.facebook.com/sharer.php?u={{ Request::url() }}" class="fb" title="Join us on Facebook"><i class="fa fa-facebook"
                                        aria-hidden="true"></i></a>
                                <a href="https://twitter.com/intent/tweet?text=di%20KLIK%20guys%20{{ Request::url() }}" class="tw" title="Join us on Twitter"><i class="fa fa-twitter"
                                        aria-hidden="true"></i></a>
                                <a href="mailto:?Subject=%20&Body=di%20KLIK%20guys%20{{ Request::url() }}" class="g-plus" title="Join us on Gmail"><i class="fa fa-at"
                                        aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div> --}}

 <!-- Sharingbutton Facebook -->
<a class="resp-sharing-button__link" href="https://facebook.com/sharer/sharer.php?u={{ Request::url() }}" target="_blank" rel="noopener" aria-label="Facebook">
    <div class="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--medium"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0C5.38 0 0 5.38 0 12s5.38 12 12 12 12-5.38 12-12S18.62 0 12 0zm3.6 11.5h-2.1v7h-3v-7h-2v-2h2V8.34c0-1.1.35-2.82 2.65-2.82h2.35v2.3h-1.4c-.25 0-.6.13-.6.66V9.5h2.34l-.24 2z"/></svg></div>Facebook</div>
  </a>
  
  <!-- Sharingbutton Pinterest -->
  <a class="resp-sharing-button__link" href="https://pinterest.com/pin/create/button/?url={{ Request::url() }}&amp;media=https://res.cloudinary.com/duh6epdw5/image/upload/{{ $post['thumbnail'] }}&amp;description=lets%20read%20my%20article%20guys." target="_blank" rel="noopener" aria-label="Pinterest">
    <div class="resp-sharing-button resp-sharing-button--pinterest resp-sharing-button--medium"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0C5.38 0 0 5.38 0 12s5.38 12 12 12 12-5.38 12-12S18.62 0 12 0zm1.4 15.56c-1 0-1.94-.53-2.25-1.14l-.65 2.52c-.4 1.45-1.57 2.9-1.66 3-.06.1-.2.07-.22-.04-.02-.2-.32-2 .03-3.5l1.18-5s-.3-.6-.3-1.46c0-1.36.8-2.37 1.78-2.37.85 0 1.25.62 1.25 1.37 0 .85-.53 2.1-.8 3.27-.24.98.48 1.78 1.44 1.78 1.73 0 2.9-2.24 2.9-4.9 0-2-1.35-3.5-3.82-3.5-2.8 0-4.53 2.07-4.53 4.4 0 .5.1.9.25 1.23l-1.5.82c-.36-.64-.54-1.43-.54-2.28 0-2.6 2.2-5.74 6.57-5.74 3.5 0 5.82 2.54 5.82 5.27 0 3.6-2 6.3-4.96 6.3z"/></svg></div>Pinterest</div>
  </a>
  
  <!-- Sharingbutton LinkedIn -->
  <a class="resp-sharing-button__link" href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{ Request::url() }}&amp;title=lets%20read%20my%20article%20guys.&amp;summary=Super%20fast%20and%20easy%20Social%20Media%20Sharing%20Buttons.%20No%20JavaScript.%20No%20tracking.&amp;source=http%3A%2F%2Fsharingbuttons.io" target="_blank" rel="noopener" aria-label="LinkedIn">
    <div class="resp-sharing-button resp-sharing-button--linkedin resp-sharing-button--medium"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
      <svg version="1.1" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
          <path d="M12,0C5.383,0,0,5.383,0,12s5.383,12,12,12s12-5.383,12-12S18.617,0,12,0z M9.5,16.5h-2v-7h2V16.5z M8.5,7.5 c-0.553,0-1-0.448-1-1c0-0.552,0.447-1,1-1s1,0.448,1,1C9.5,7.052,9.053,7.5,8.5,7.5z M18.5,16.5h-3V13c0-0.277-0.225-0.5-0.5-0.5 c-0.276,0-0.5,0.223-0.5,0.5v3.5h-3c0,0,0.031-6.478,0-7h3v0.835c0,0,0.457-0.753,1.707-0.753c1.55,0,2.293,1.12,2.293,3.296V16.5z" />
      </svg></div>LinkedIn</div>
  </a>
  
  <!-- Sharingbutton Telegram -->
  <a class="resp-sharing-button__link" href="https://telegram.me/share/url?text=lets%20read%20my%20article%20guys.&amp;url={{ Request::url() }}" target="_blank" rel="noopener" aria-label="Telegram">
    <div class="resp-sharing-button resp-sharing-button--telegram resp-sharing-button--medium"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 23.5c6.35 0 11.5-5.15 11.5-11.5S18.35.5 12 .5.5 5.65.5 12 5.65 23.5 12 23.5zM2.505 11.053c-.31.118-.505.738-.505.738s.203.62.513.737l3.636 1.355 1.417 4.557a.787.787 0 0 0 1.25.375l2.115-1.72a.29.29 0 0 1 .353-.01L15.1 19.85a.786.786 0 0 0 .746.095.786.786 0 0 0 .487-.573l2.793-13.426a.787.787 0 0 0-1.054-.893l-15.568 6z" fill-rule="evenodd"/></svg></div>Telegram</div>
  </a>
  
  


                    <!-- Related Post Area -->
                    <div class="related-posts clearfix">
                        <!-- Line -->
                        <div class="curve-line bg-img mb-50"
                            style="background-image: url({{ asset('img/breadcrumb-line.png') }});"></div>
                        <!-- Line -->
                        <div class="curve-line bg-img" style="background-image: url({{ asset('img/breadcrumb-line.png') }});">
                        </div>
                    </div>

                    <!-- Comment Area Start -->
                    <div class="comment_area clearfix">
                        <h4 class="headline"> {{ count($post['comments']) }} Komentar</h4>
                        
                        <ol>
                            @for ($i = 0; $i < count($comments); $i++) <li class="single_comment_area">
                                <div class="comment-wrapper d-flex">
                                    <!-- Comment Meta -->
                                    <div class="comment-author">
                                        <img src="https://res.cloudinary.com/duh6epdw5/image/upload/v1590217083/{{ $post['thumbnail'] }}"
                                            class="rounded-circle" alt="">
                                    </div>
                                    <!-- Comment Content -->
                                    <div class="comment-content">
                                        <span
                                            class="comment-date">{{ date("d M Y | l", strtotime($comments[$i]['updated_at']))  }}
                                            | {{ $comments[$i]['users']['username'] }}</span>
                                        <h5></h5>
                                        <p>{{ $comments[$i]['content'] }}</p>
                                        {{-- <a href="#">Like</a>
                                        <a class="active" href="#">Reply</a> --}}
                                    </div>
                                </div>
                        </li>
                            @endfor

                        </ol>
                        {{ $comments->links() }}
                    </div>


                            <!-- Leave A Comment -->
                        <div class="leave-comment-area clearfix">
                            <div class="comment-form">
                                @if (Auth::user())
                                    <h4 class="headline text-capitalize">hi {{ $userLogged['username'] }}, komen di bawah yuk..</h4>
                                                                    <!-- Comment Form -->
                                    <form action="/comments" method="post">
                                        @csrf
                                        <div class="row">
                                            <input type="text" value="{{ $post['id'] }}" name="post_id" hidden>
                                            @if (Auth::user())
                                                <input type="text" value="{{ $userLogged['id'] }}" name="author_id" hidden>                                       
                                            @endif
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <textarea class="form-control" name="content" id="content" cols="30"
                                                        rows="10" placeholder="Comment"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <button type="submit" class="btn foode-btn">Post Comment</button>
                                            </div>
                                        </div>
                                    </form>
                                @else
                                    <h4 class="headline text-capitalize">login dulu untuk berkomentar</h4>
                                @endif
                            </div>
                        </div>
                    
                  

                </>
            </div>

            <!-- Blog Sidebar Area -->
        </div>
        <div class="col-12 col-sm-9 col-md-6 col-lg-4">
        <div class="post-sidebar-area">

                <!-- ##### Single Widget Area ##### -->
                <div class="single-widget-area mt-0">
                    <!-- Title -->
                    <div class="widget-title">
                        <h6>Categories</h6>
                    </div>
                    <ol class="foode-catagories">
                        @if( count($post['categoriesName']) > 0 )
                            @for ($i = 0; $i < count($post['categoriesName']); $i++)
                                <li>
                                    <a href="/category/{{ $post['categoriesName'][$i]['category_name'] }}">
                                        <span><i class="fa fa-stop" aria-hidden="true"></i>
                                            {{ $post['categoriesName'][$i]['category_name'] }}</span>
                                        <span>{{ amountCategory($post['categoriesName'][$i]['id']) }}</span>
                                    </a>
                                </li>
                            @endfor
                        @else
                        <li>category not found</li>
                        @endif
                    </ol>
                </div>

                <!-- ##### Single Widget Area ##### -->
                <div class="single-widget-area">
                    <!-- Title -->
                    <div class="widget-title">
                        <h6>Latest Posts</h6>
                    </div>

                    @for ($i = 0; $i < count($postUser); $i++) <!-- Single Latest Posts -->
                        <div class="single-latest-post d-flex">
                            <div class="post-thumb">
                                <img src="https://res.cloudinary.com/duh6epdw5/image/upload/{{ $postUser[$i]['thumbnail'] }}"
                                    alt="img not loaded">
                            </div>
                            <div class="post-content">
                                <a href="/single_post/{{ $postUser[$i]['slug'] }}" class="post-title">
                                    <h6>{{ $postUser[$i]['title'] }}</h6>
                                </a>
                                <a href="/profil/{{ $postUser[$i]['users']['username'] }}" class="post-author"><span>by</span> {{ $postUser[$i]['users']['username'] }}</a>
                            </div>
                        </div>
                        @endfor

                </div>

                <!-- ##### Single Widget Area ##### -->
                <div class="single-widget-area">
                    <!-- Adds -->
                    <a href="#"><img src="img/blog-img/add.png" alt=""></a>
                </div>

                
                <!-- ##### Single Widget Area ##### -->
                <div class="single-widget-area">
                    <!-- Title -->
                    <div class="widget-title">
                        <h6>popular tags</h6>
                    </div>
                    
                    <!-- Tags -->
                    <ol class="popular-tags d-flex flex-wrap">
                        @if ( count($tags) > 0 )
                            @for ($i = 0; $i < count($tags); $i++) 
                            <li>
                                <a href="#"> {{ $tags[$i] }} </a>
                            </li>
                            @endfor
                        @else
                            <li style="text-align: center">tag not found</li>
                        @endif
                    </ol>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- ##### Blog Content Area End ##### -->

@endsection


@push('script')
    <script>
        $( () => {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
            });

            window.onscroll = function () 
            {
                toggleLike()
                console.log('hello world');
            }; 
            toggleLike = () => 
            {
                let body = document.documentElement.scrollTop;
                console.log(body);
                if (body > 260) 
                {
                    $('.content-2').css('display', 'block');
                    console.log("show");
                } 
                else 
                {
                    $('.content-2').css('display', 'none');
                    console.log('none');
                }
            }

            liker = (liker) => {
                $.post('/'+liker, { 'user_id':userId, 'post_id':postId}, function(data){
                    console.log(data);
                    $('#count-like').html(data);
                });
            }

            let no = 0;
            let like = $('#checkbox1');
            let userId = $('#userId').text();
            let postId = $('#postId').text();

            document.addEventListener('keydown', function(key){
                if(key.key === 'l' || key.key === 'L') {
                    let a1 = $('#a1').text();
                        if (a1 == ' like ') {
                            liker('like');
                            like.prop("checked", "checked");
                                $('#a1').text('liked');       
                        } else {
                            liker('unlike');
                            like.removeAttr("checked");
                            $('#a1').text(' like ');
                        }
                }
            });

     
            // let following = ;
        
            like.click( () => {
                let a1 = $('#a1').text();
                if(a1 == ' like ') {
                    liker('like');
                    $('#a1').text('liked');
                    console.log('like');
                } else {
                    liker('unlike');
                    $('#a1').text(' like ');
                    console.log('unlike');
                }
            });

        });
    </script>
@endpush