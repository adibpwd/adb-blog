@extends('layouts.main')

@section('title', 'profile')

@section('head')
<link rel="stylesheet" href="{{ asset('css/profile.css') }}">
@endsection

@section('content')

    {{-- {{ dd("https://res.cloudinary.com/duh6epdw5/image/upload/{{ $posts[3]['thumbnail']"}}); }} --}}
    
    <div class="container-fluid text-center profile">
        <img src="{{ asset('img/game-of-thrones.jpg') }}" alt="game" class="w-100 rounded-b20 header-image">
        <img src="{{ asset('img/logo.jpg') }}" alt="logo" class="rounded-circle">
        <div class="col-md-12 d-flex justify-content-center">
            <h2 style="margin: 10% 0 0 0; font-weight: bold;">{{ $user['username'] }}</h2>
            {{-- <h6 style="width: 30%; height: 20px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; margin: 13% -10% 0 -20%;">description here Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quasi fugiat facere itaque exercitationem veniam quo aliquid necessitatibus officia sit commodi! </h6> --}}
        </div>
    </div>

    {{-- detail github, wakatime dll --}}
    {{-- <div class="box-shadow">
        <div class="container detail">
            <div class="row justify-content-center">
                <div class="card1" style="margin: 0.2%;">
                    <h2>North</h2>
                    <i class="i fas fa-arrow-right"></i>
                    <p>a lonely trip.</p>
                    <div class="pic"></div>
                    <ul>
                        <li></li>
                    </ul>
                    <div class="social">
                        <i class="i fab fa-facebook-f"></i>
                        <i class="i fab fa-twitter"></i>
                        <i class="i fab fa-instagram"></i>
                        <i class="i fab fa-github"></i>
                    </div>
                    <button>
                    </button>
                </div>
                <div class="card1" style="margin: 0.2%;">
                    <h2>North</h2>
                    <i class="i fas fa-arrow-right"></i>
                    <p>a lonely trip.</p>
                    <div class="pic"></div>
                    <ul>
                        <li></li>
                    </ul>
                    <div class="social">
                        <i class="i fab fa-facebook-f"></i>
                        <i class="i fab fa-twitter"></i>
                        <i class="i fab fa-instagram"></i>
                        <i class="i fab fa-github"></i>
                    </div>
                    <button>
                    </button>
                </div>
                <div class="card1" style="margin: 0.2%;">
                    <h2>North</h2>
                    <i class="i fas fa-arrow-right"></i>
                    <p>a lonely trip.</p>
                    <div class="pic"></div>
                    <ul>
                        <li></li>
                    </ul>
                    <div class="social">
                        <i class="i fab fa-facebook-f"></i>
                        <i class="i fab fa-twitter"></i>
                        <i class="i fab fa-instagram"></i>
                        <i class="i fab fa-github"></i>
                    </div>
                    <button>
                    </button>
                </div>
                <div class="card1" style="margin: 0.2%;">
                    <h2>North</h2>
                    <i class="i fas fa-arrow-right"></i>
                    <p>a lonely trip.</p>
                    <div class="pic"></div>
                    <ul>
                        <li></li>
                    </ul>
                    <div class="social">
                        <i class="i fab fa-facebook-f"></i>
                        <i class="i fab fa-twitter"></i>
                        <i class="i fab fa-instagram"></i>
                        <i class="i fab fa-github"></i>
                    </div>
                    <button>
                    </button>
                </div>
            </div>
        </div>
    </div> --}}

    <div class="container-fluid mt-3 post-article">
        <div class="row justify-content-center">

            @for ($i = 0; $i < $posts->count(); $i++)
            <div class="card5 box-shadow" style="background-image: url(https://res.cloudinary.com/duh6epdw5/image/upload/{{ $posts[$i]['thumbnail'] }});">
                <section class="center" style="background-color: black; opacity: 0.7;">
                    <div class="about_movie">
                        <h3> {{ Str::limit($posts[$i]['title'], 45) }} </h3>
                        <div class="movie_info justify-content-center">
                            <p> {{ date('d M Y | D', strtotime($posts[$i]['updated_at'])) }} </p>
                        </div>
                        <div class="movie_desc">
                            <p>{{ strip_tags(Str::limit($posts[$i]['content'], 130)) }}</p>
                        </div>
        
                        <button class="watch"><i class="fab fa-accessible-icon"></i> Lihat Post</button>
                        {{-- <button class="watchlist">
                            <svg viewBox="0 0 401.994 401.994"
                                style="enable-background:new 0 0 401.994 401.994;" xml:space="preserve">
                                <path
                                    d="M394,154.175c-5.331-5.33-11.806-7.994-19.417-7.994H255.811V27.406c0-7.611-2.666-14.084-7.994-19.414
                      C242.488,2.666,236.02,0,228.398,0h-54.812c-7.612,0-14.084,2.663-19.414,7.993c-5.33,5.33-7.994,11.803-7.994,19.414v118.775
                      H27.407c-7.611,0-14.084,2.664-19.414,7.994S0,165.973,0,173.589v54.819c0,7.618,2.662,14.086,7.992,19.411
                      c5.33,5.332,11.803,7.994,19.414,7.994h118.771V374.59c0,7.611,2.664,14.089,7.994,19.417c5.33,5.325,11.802,7.987,19.414,7.987
                      h54.816c7.617,0,14.086-2.662,19.417-7.987c5.332-5.331,7.994-11.806,7.994-19.417V255.813h118.77
                      c7.618,0,14.089-2.662,19.417-7.994c5.329-5.325,7.994-11.793,7.994-19.411v-54.819C401.991,165.973,399.332,159.502,394,154.175z" />
                            </svg>
                            Add to watchlist!</button> --}}
                    </div>
                </section>
        
        
                <svg class="wavy" viewBox="0 0 500 500" preserveAspectRatio="xMinYMin meet">
                    <path d="M0,100 C150,200 350,0 500,100 L500,00 L0,0 Z" style="stroke: none;"></path>
                </svg>
            </div>
            @endfor 
        </div>
    </div>
    <br>
    <br>
    <br>
    {{ $posts->links() }}


@endsection