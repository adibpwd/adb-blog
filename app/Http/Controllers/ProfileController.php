<?php

namespace App\Http\Controllers;

// use \Request;
use App\Post;
use App\Sosmed;
use App\User;
use Response;
use App\Follower;
use Illuminate\Http\Request;
use JD\Cloudder\Facades\Cloudder;
use Illuminate\Support\Facades\Auth;
class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index($username) jika udah buat tampilan terus manggil dri db
    public function index($username)
    { 
        $user = User::where('username', $username)->with('sosmed')->first();
        $followers = Follower::where('followed', $user['id'])->where('following', Auth::user()->id)->first();
        $amount_followers = Follower::where('followed', $user['id'])->count();
        if($followers) {
            $followers = 'followed';
        } else {
            $followers = 'follow';
        }
        // dd($amount_followers);
        if ($user) {
            $posts = Post::where('author_id', $user['id'])->paginate(11);
            $less = 11 - count($posts);
            // for ($i=0; $i < $less; $i++) { 
            //     $
            // }
            // dd($posts);
            return view('profile_foode', compact('posts', 'user', 'followers', 'amount_followers'));
        } else {
            $user = 'user not found';
            return view('profile_foode', compact('user'));
        }
        // dd($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($username)
    {
        // dd($username);
        $user = User::where('username', $username)->with('sosmed')->first();
        return view('edit_profile_foode', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {        
        $r = $request;
        $foto_profil = $r->file('foto_profil');
        $cover = $r->file('cover');
        $no_telp = $r['no_telp'];
        $tgl_lahir = $r['tgl_lahir'];
        
      
        request()->validate([
            'username' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'no_telp' => 'sometimes|nullable|digits:10,15',
            'tgl_lahir' => 'sometimes|nullable|date_format:Y-m-d',
            'foto_profil' => 'sometimes|nullable|max:1000',
            'cover' => 'sometimes|nullable|max:1500',
        ],
            [
                'username.required' => 'username belum di isi!!!',
                'username.min' => 'username belum 5 huruf atau lebih',
                'username.string' => 'username bukan string',
                'username.unique' => 'username sudah ada pakai username lain',
                'email.required' => 'email belum di isi!!!',
                'email.string' => 'emailnya bukan string',
                'email.email' => 'bukan email yang di isikan',
                'email.max' => 'email max 255 huruf, coba hapus beberapa',
                'no_telp.digits' => 'harus angka minimal 10 angka dan maximal 15 angka',
                'tgl_lahir.date_format' => 'contoh tanggal yang benar : 2000-07-19.',
                'foto_profil.max' => 'foto profil maximal 1mb..kecilin size fotonya dulu y..',
                'cover.max' => 'foto cover maximal 1.5mb..kecilin size fotonya dulu y..',
            ]);

     
                // dd('');

            if ( $foto_profil != null) {
                ?> <script> console.log('foto profil');
                    </script> <?php
                    $mimeFile = $foto_profil->getMimeType();
                    $upload = \Cloudder::upload($foto_profil);
                    $result = Cloudder::getResult();
                    $foto_profil_cloudinary = 'v' . $result['version'] . '/' .Cloudder::getPublicId() . '.' . substr($mimeFile, 6);
            } else {
                    $foto_profil_cloudinary = 'v1590921012/account_default.jpg';
            }
            
            if ( $cover != null) {
                ?> <script> console.log('foto profil');
                    </script> <?php
                    $mimeFile = $cover->getMimeType();
                    $upload = \Cloudder::upload($cover);
                    $result = Cloudder::getResult();
                    $cover_cloudinary = 'v' . $result['version'] . '/' .Cloudder::getPublicId() . '.' . substr($mimeFile, 6);
            } else {
                    $cover_cloudinary = 'v1590934350/img-not-found1.jpg';
            }

        User::where('id', $r['user_id'])->update([
            'username' => $r['username'],
            'no_telp' => $r['no_telp'],
            'email' => $r['email'],
            'tgl_lahir' => $r['tgl_lahir'],
            'foto_profil' => $foto_profil_cloudinary,
            'foto_header' => $cover_cloudinary
        ]);

        Sosmed::where('id', $r['sosmed_id'])->update([
            'facebook' => $r['facebook'],
            'twitter' => $r['twitter'],
            'instagram' => $r['instagram'],
            'linkedin' => $r['linkedin'],
            'youtube' => $r['youtube'],
        ]);

        $path = "profil/" . $r['username'];

        return redirect($path);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function foll(Request $request) {
        // if(Request::ajax()) {
        //     return Response::json(Request::all());
        // }
        Follower::create([
            'followed' => $request['followed'],
            'following' => $request['following']
        ]);
        $amount_followers = Follower::where('followed', $request['followed'])->count();
        return $amount_followers;
    }

    public function unfoll(Request $request){
        Follower::where('followed', $request['followed'])->where('following', $request['following'])->delete();
        $amount_followers = Follower::where('followed', $request['followed'])->count();
        return $amount_followers;
    }

}
