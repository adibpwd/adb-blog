<?php

use App\Post;
use Illuminate\http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */



Route::group(['middleware' => 'auth'], function () {

    Route::post('/editor', 'PostController@store');
    Route::post('ckeditor/image_upload', 'PostController@upload')->name('upload');
    Route::get('/dashboard/{username}', 'PostController@index');
    Route::get('/editor', function () {
        return redirect('/dashboard');
    });
    
    Route::get('/edit post/{id}', 'PostController@editPost');
    Route::get('/new post', 'PostController@newPost');
    Route::get('/delete/{id}', 'PostController@destroy');

    Route::get('/profil/{username}', 'ProfileController@index');
    Route::get('/edit_profil/{profil}', 'ProfileController@edit');
    Route::post('/edit_profil', 'ProfileController@update');
    Route::post('/update post', 'PostController@update');
    Route::post('/foll', 'ProfileController@foll');
    Route::post('/unfoll', 'ProfileController@unfoll');
    Route::post('/like', 'PostController@like');
    Route::post('/unlike', 'PostController@unlike');
});

Route::group(['middleware' => 'guest'], function () {
    Route::get('/user/{loginregister}', 'LoginRegisterController@index');
});
 
Route::get('/home', 'HomeController@index');
Route::get('/', function() {
    return redirect('/home');; 
}); 
Route::get('/single_post/{slug}', 'PostController@onePost');
Route::post('/comments', 'CommentController@store');
Route::get('/category/{categoryName}', 'CategoryController@index');

// Route::post('/upload', function (Request $request) {
//     dd($request->file('thing'));
// });

// Route::get('/sosmed', function () {
//     return view('sosmed');
// });
 
// Route::get('/header', function () {
//     return view('header-home');
// });

// Route::get('/profile', function () {
//     return view('profile');
// });

// Route::get('/article', function () {
//     return view('article');
// });



// Route::get('postest', 'CKEditorController@index');

// Route::get('/profilTest', 'ProfileController@index');
// Route::get('/', 'CloudderController@index');
// Route::post('/', 'CloudderController@upload');
// Route::get('/{namafile}', 'CloudinaryController@hasil');
Auth::routes();
