@extends('layouts.main')

@section('title', 'title article')

@section('content')
    <div class="container3" style="margin-top: 10%;">
        <div class="row">
            <div class="col-md-8">
                <div class="mt-1 rounded box-shadow p-2">
                    <h3>hello world</h3>
                    <h3>hello world</h3>
                    <h3>hello world</h3>
                    <h3>hello world</h3>
                    <h3>hello world</h3>
                    <h3>hello world</h3>
                    <h3>hello world</h3>
                    <h3>hello world</h3>
                    <h3>hello world</h3>
                    <h3>hello world</h3>
                    <h3>hello world</h3>
                    <h3>hello world</h3>
                </div>
            </div>
            <div class="col-md-4">
                <div class="mt-1 rounded box-shadow p-3">
                    <!-- this is the markup. you can change the details (your own name, your own avatar etc.) but don’t change the basic structure! -->
                    <aside class="profile-card">
                        <header>
                            <!-- here’s the avatar -->
                            <a href="https://tutsplus.com">
                                <img src="{{ asset('img/logo.jpg') }}">
                            </a>

                            <!-- the username -->
                            <h1>George Darkos</h1>

                            <!-- and role or location -->
                            <h2>- Full Stack Web Developer -</h2>

                        </header>

                        <!-- bit of a bio; who are you? -->
                        <div class="profile-bio">

                            <p>Hello there!</p>
                            <p>I am a full stack web developer. I mainly work with PHP, HTML, CSS, JS and WordPress.
                                <br />I also play well with Photoshop, Corel Draw, After Effects and other cool stuff.
                            </p>

                        </div>

                        <!-- some social links to show off -->
                        <ul class="profile-social-links">

                            <!-- twitter - el clásico  -->
                            <li>
                                <a href="https://twitter.com/tutsplus">
                                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/210284/social-twitter.svg">
                                </a>
                            </li>

                            <!-- envato – use this one to link to your marketplace profile -->
                            <li>
                                <a href="https://envato.com">
                                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/210284/social-envato.svg">
                                </a>
                            </li>

                            <!-- codepen - your codepen profile-->
                            <li>
                                <a href="https://codepen.io/tutsplus">
                                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/210284/social-codepen.svg">
                                </a>
                            </li>

                            <!-- add or remove social profiles as you see fit -->

                        </ul>

                    </aside>
                    <!-- that’s all folks! -->
                </div>
                <div class="recomend-article">
                    <div class="col-md-13">
                        <div class="mt-3 rounded box-shadow p-2" style="width: 100%;">
                            <h6>news article</h6>
                        <div>
                    </div>
                </div>
                <div class="col-md-13">
                    <div class="mt-3 rounded box-shadow p-2" style="width; 100%;">
                        <h6>news article</h6>
                    </div>
                </div>
                <div class="col-md-13">
                    <div class="mt-3 rounded box-shadow p-2" style="width; 100%;">
                        <h6>news article</h6>
                    </div>
                </div>
                <div class="col-md-13">
                    <div class="mt-3 rounded box-shadow p-2" style="width; 100%;">
                        <h6>news article</h6>
                    </div>
                </div>
                <div class="col-md-13">
                    <div class="mt-3 rounded box-shadow p-2" style="width; 100%;">
                        <h6>news article</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <!-- comments container -->
        <div class="container" style="margin-top: 15%!important; margin-bottom: 5%!important;">
            <div class="scroll-comment">
            <!-- Comments are structured in the following way:
                {ul} defines a new comment (singular)
                {li} defines a new reply to the comment {ul}
                example:
                <ul>
                    <comment>
                    </comment
                        <li>
                            <reply>
                            </reply>
                        </li>
                        <li>
                            <reply>
                            </reply>
                        </li>
                        <li>
                            <reply>
                            </reply>
                        </li>
                </ul> -->
                <!-- used by #{user} to create a new comment -->
                <div class="create_new_comment">
    
                    <!-- current #{user} avatar -->
                    <div class="user_avatar">
                        <img src="https://s3.amazonaws.com/uifaces/faces/twitter/BillSKenney/73.jpg">
                    </div><!-- the input field -->
                    <div class="input_comment">
                        <input type="text" placeholder="Join the conversation..">
                    </div>
                </div>
                <!-- new comment -->
                <div class="new_comment">
                    <!-- build comment -->
                    <ul class="user_comment">
                        <!-- current #{user} avatar -->
                        <div class="user_avatar">
                            <img src="https://s3.amazonaws.com/uifaces/faces/twitter/dancounsell/73.jpg">
                        </div><!-- the comment body -->
                        <div class="comment_body">
                            <p>Gastropub cardigan jean shorts, kogi Godard PBR&B lo-fi locavore. Organic chillwave vinyl Neutra.
                                Bushwick Helvetica cred freegan, crucifix Godard craft beer deep v mixtape cornhole Truffaut master
                                cleanse pour-over Odd Future beard. Portland polaroid iPhone.</p>
                        </div>
                        <!-- comments toolbar -->
                        <div class="comment_toolbar">
                            <!-- inc. date and time -->
                            <div class="comment_details">
                                <ul>
                                    <li><i class="fa fa-clock-o"></i> 13:94</li>
                                    <li><i class="fa fa-calendar"></i> 04/01/2015</li>
                                    <li><i class="fa fa-pencil"></i> <span class="user">John Smith</span></li>
                                </ul>
                            </div><!-- inc. share/reply and love -->
                            <div class="comment_tools">
                                <ul>
                                    <li><i class="fa fa-share-alt"></i></li>
                                    <li><i class="fa fa-reply"></i></li>
                                    <li><i class="fa fa-heart love"></i></li>
                                </ul>
                            </div>
                        </div>
                        <!-- start user replies -->
                        <li>
                            <!-- current #{user} avatar -->
                            <div class="user_avatar">
                                <img src="https://s3.amazonaws.com/uifaces/faces/twitter/manugamero/73.jpg">
                            </div><!-- the comment body -->
                            <div class="comment_body">
                                <p>
                                    <div class="replied_to">
                                        <p><span class="user">John Smith:</span>Gastropub cardigan jean shorts, kogi Godard PBR&B
                                            lo-fi locavore. Organic chillwave vinyl Neutra. Bushwick Helvetica cred freegan,
                                            crucifix Godard craft beer deep v mixtape cornhole Truffaut master cleanse pour-over Odd
                                            Future beard. Portland polaroid iPhone.</p>
                                    </div>That's exactly what I was thinking!
                                </p>
                            </div>
    
                            <!-- comments toolbar -->
                            <div class="comment_toolbar">
                                <!-- inc. date and time -->
                                <div class="comment_details">
                                    <ul>
                                        <li><i class="fa fa-clock-o"></i> 14:52</li>
                                        <li><i class="fa fa-calendar"></i> 04/01/2015</li>
                                        <li><i class="fa fa-pencil"></i> <span class="user">Andrew Johnson</span></li>
                                    </ul>
                                </div><!-- inc. share/reply and love -->
                                <div class="comment_tools">
                                    <ul>
                                        <li><i class="fa fa-share-alt"></i></li>
                                        <li><i class="fa fa-reply"></i></li>
                                        <li><i class="fa fa-heart love"><span class="love_amt"> 4</span></i></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <!-- start user replies -->
                        <li>
                            <!-- current #{user} avatar -->
                            <div class="user_avatar">
                                <img src="https://s3.amazonaws.com/uifaces/faces/twitter/ManikRathee/73.jpg">
                            </div><!-- the comment body -->
                            <div class="comment_body">
                                <p>
                                    <div class="replied_to">
                                        <p><span class="user">John Smith:</span>Gastropub cardigan jean shorts, kogi Godard PBR&B
                                            lo-fi locavore. Organic chillwave vinyl Neutra. Bushwick Helvetica cred freegan,
                                            crucifix Godard craft beer deep v mixtape cornhole Truffaut master cleanse pour-over Odd
                                            Future beard. Portland polaroid iPhone.</p>
                                    </div>Finally someone who actually gets it!<div class="replied_to">
                                        <p><span class="user">Andrew Johnson:</span>That's exactly what I was thinking!</p>
                                    </div>That's awesome!
                                </p>
                            </div>
                            <!-- comments toolbar -->
                            <div class="comment_toolbar">
                                <!-- inc. date and time -->
                                <div class="comment_details">
                                    <ul>
                                        <li><i class="fa fa-clock-o"></i> 14:59</li>
                                        <li><i class="fa fa-calendar"></i> 04/01/2015</li>
                                        <li><i class="fa fa-pencil"></i> <span class="user">Simon Gregor</span></li>
                                    </ul>
                                </div><!-- inc. share/reply and love -->
                                <div class="comment_tools">
                                    <ul>
                                        <li><i class="fa fa-share-alt"></i></li>
                                        <li><i class="fa fa-reply"></i></li>
                                        <li><i class="fa fa-heart love"><span class="love_amt"> 4039</span></i></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
    
                <!-- new comment -->
                <div class="new_comment">
                    <!-- build comment -->
                    <ul class="user_comment">
                        <!-- current #{user} avatar -->
                        <div class="user_avatar">
                            <img src="https://s3.amazonaws.com/uifaces/faces/twitter/kfriedson/73.jpg">
                        </div><!-- the comment body -->
                        <div class="comment_body">
                            <p>Gastropub cardigan jean shorts, kogi Godard PBR&B lo-fi locavore. Organic chillwave vinyl Neutra.
                                Bushwick Helvetica cred freegan, crucifix Godard craft beer deep v mixtape cornhole Truffaut master
                                cleanse pour-over Odd Future beard. Portland polaroid iPhone.</p>
                        </div>
                        <!-- comments toolbar -->
                        <div class="comment_toolbar">
                            <!-- inc. date and time -->
                            <div class="comment_details">
                                <ul>
                                    <li><i class="fa fa-clock-o"></i> 13:94</li>
                                    <li><i class="fa fa-calendar"></i> 04/01/2015</li>
                                    <li><i class="fa fa-pencil"></i> <span class="user">Sarah Walkman</span></li>
                                </ul>
                            </div><!-- inc. share/reply and love -->
                            <div class="comment_tools">
                                <ul>
                                    <li><i class="fa fa-share-alt"></i></li>
                                    <li><i class="fa fa-reply"></i></li>
                                    <li><i class="fa fa-heart love"></i></li>
                                </ul>
                            </div>
                        </div>
                        <!-- start user replies -->
                        <li>
                            <!-- current #{user} avatar -->
                            <div class="user_avatar">
                                <img src="https://s3.amazonaws.com/uifaces/faces/twitter/cbillins/73.jpg">
                            </div><!-- the comment body -->
                            <div class="comment_body">
                                <p>
                                    <div class="replied_to">
                                        <p><span class="user">Sarah Walkman:</span>Pork belly migas flexitarian messenger bag
                                            Brooklyn gluten-free. Tilde kitsch skateboard Helvetica, lumbersexual four loko direct
                                            trade pour-over. Cronut deep v keffiyeh cornhole food truck</p>
                                    </div>I'm tired, does anybody know a good place to buy extra strength coffee?
                                </p>
                            </div>
                            <!-- comments toolbar -->
                            <div class="comment_toolbar">
                                <!-- inc. date and time -->
                                <div class="comment_details">
                                    <ul>
                                        <li><i class="fa fa-clock-o"></i> 19:23</li>
                                        <li><i class="fa fa-calendar"></i> 14/01/2015</li>
                                        <li><i class="fa fa-pencil"></i> <span class="user">Blake Anderson</span></li>
                                    </ul>
                                </div><!-- inc. share/reply and love -->
                                <div class="comment_tools">
                                    <ul>
                                        <li><i class="fa fa-share-alt"></i></li>
                                        <li><i class="fa fa-reply"></i></li>
                                        <li><i class="fa fa-heart love"><span class="love_amt"> 12</span></i></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
@endsection
