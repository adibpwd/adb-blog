<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
           'adib', 'alpin'
        ];

        $no_user = [
           '6281477103785', '6282198391258'
        ];

        for ($i0; $i < count($user); $i++) {
            $tgl_random = rand(1, 29); 
            $bln_random = rand(1, 12); 
            DB::table('users')->insert([
                'username' => $user[$i],
                'password' => 'sarirejo',
                'no_telp' => $no_user[$i],
                'email' => $user[$i] . 'tambak@gmail.com',
                'tgl_lahir' => '2000-' . $bln_random . '-' . $tgl_random,
            ]);
        }
    }
}
