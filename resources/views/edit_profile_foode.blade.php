@extends('layouts.foode')

@section('head')
    <link href="{{ asset('css/style2.css') }}" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Montserrat:700' rel='stylesheet' type='text/css'>
@endsection



@section('content')

    {{-- @if(session()->get('errors'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ dd($errors) }}</strong>
        </span>    
    @endif --}}

    <br><br><br><br><br><br><br><br>
    <form action="/edit_profil" method="post" enctype="multipart/form-data" id="formEditProfile">
        @csrf
        <div class="container-fluid">
            <div class="row  editProfile d-flex justify-content-center">
                @if ($user->sosmed)
                    <input type="text" value="{{ $user->sosmed->id }}" name="sosmed_id" hidden>                
                @endif
                <input type="text" value="{{ $user->id }}" name="user_id" hidden>
              
                <span class="col-10 col-sm-6 col-md-5 text-center">
                        <span style="position: relative; width: 100px; height: 100px;">
                        <img id="foto_profil1" alt="" width="100" height="100" name="mantap"/>
                        <p
                            style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); font-size: 70%; font-family: 'Chelsea Market'; text-align: center; background-color: white; width: 98%; opacity: 0.8;">
                            Foto Profil</p>
                        </span>
                    <input type="file" class="@error('foto_profil') is-invalid @enderror"
                        onchange="document.getElementById('foto_profil1').src = window.URL.createObjectURL(this.files[0])" name="foto_profil"
                        id="foto_profil" hidden>
                    @error('foto_profil')
                        <strong style="color: red; font-size: 80%;">{{ $message }}</strong>
                    @enderror
                        <br>
                    <label for="foto_profil" class="sel-file bg-gradient-blue w-25" name="foto_profil">Select Foto Profil</label><br><br>
                </span>
                <span class="col-10 col-sm-6 col-md-5 text-center">
                        <span style="position: relative; width: 100px; height: 100px;">
                        <img id="cover1" alt="" width="100" height="100" name="mantap1"/>
                        <p
                            style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); font-size: 70%; font-family: 'Chelsea Market'; text-align: center; background-color: white; width: 98%; opacity: 0.8;">
                            Cover</p>
                        </span>
                    <input type="file" class="@error('cover') is-invalid @enderror"
                        onchange="document.getElementById('cover1').src = window.URL.createObjectURL(this.files[0])" name="cover"
                        id="cover" hidden>
                    @error('cover')
                        <strong style="color: red; font-size: 80%;">{{ $message }}</strong>
                    @enderror
                        <br>
                    <label for="cover" class="sel-file bg-gradient-blue w-25" name="cover">Select Cover</label><br><br>
                </span>
    
                <span class="col-10 col-sm-6 col-md-3">
                    <input class="gate w-100 pt-3 pb-3 @error('username') is-invalid @enderror" id="username" type="text" name="username" placeholder="Username" value="{!! Request::old('name', $user->username) !!}"/><label class="w-50" for="name">Username</label>
                    @error('username')
                        <strong style="color: red; font-size: 80%;">{{ $message }}</strong>
                    @enderror 
                </span> 
                <span class="col-10 col-sm-5 col-md-3">
                    <input class="gate w-100 pt-3 pb-3 @error('number') is-invalid @enderror" id="number" type="text" name="no_telp" placeholder="Num Telp / 6282982432734" value="{!! Request::old('no_telp', $user->no_telp) !!}"/><label class="w-50"
                        for="age">Number</label>
                        @error('no_telp')
                            <strong style="color: red; font-size: 80%;">{{ $message }}</strong>
                        @enderror
                </span>
                <span class="col-10 col-sm-5 col-md-3">
                    <input class="gate w-100 pt-3 pb-3 @error('email') is-invalid @enderror" id="email" type="text" placeholder="Email" name="email" value="{!! Request::old('email', $user->email) !!}"/><label class="w-50" for="class">Email</label>
                    @error('email')
                        <strong style="color: red; font-size: 80%;">{{ $message }}</strong>
                    @enderror
                </span>
                <span class="col-10 col-sm-5 col-md-3"> 
                    <input class="gate w-100 pt-3 pb-3 @error('tgl_lahir') is-invalid @enderror" id="tgl_lahir" type="text" placeholder="YYYY-MM-DD / 2000-07-19" name="tgl_lahir" value="@if ($user->sosmed) {!! Request::old('tgl_lahir', $user->tgl_lahir) !!}@endif"/><label class="w-50"
                        for="english">Tgl Lahir</label>
                        @error('tgl_lahir')
                            <strong style="color: red; font-size: 80%;">{{ $message }}</strong>
                        @enderror
                </span>
                <span class="col-10 col-sm-5 col-md-3">
                    <input class="gate w-100 pt-3 pb-3 @error('facebook') is-invalid @enderror" id="facebook" type="text" placeholder="URL Facebook Account" name="facebook" value="@if ($user->sosmed) {!! Request::old('facebook', $user->sosmed->facebook) !!}@endif"/><label class="w-50" for="card">Facebook</label>
                    @error('facebook')
                        <strong style="color: red; font-size: 80%;">{{ $message }}</strong>
                    @enderror
                </span>
                <span class="col-10 col-sm-5 col-md-3">
                    <input class="gate w-100 pt-3 pb-3 @error('twitter') is-invalid @enderror" id="twitter" type="text" placeholder="URL Twitter Account" name="twitter" value="@if ($user->sosmed) {!! Request::old('twitter', $user->sosmed->twitter) !!}@endif"/><label class="w-50" for="knock">Twitter</label>
                    @error('twitter')
                        <strong style="color: red; font-size: 80%;">{{ $message }}</strong>
                    @enderror
                </span>
                <span class="col-10 col-sm-5 col-md-3">
                    <input class="gate w-100 pt-3 pb-3 @error('instagram') is-invalid @enderror" id="instagram" type="text" placeholder="URL Instagram Account" name="instagram" value="@if ($user->sosmed) {!! Request::old('instagram', $user->sosmed->instagram) !!}@endif"/><label class="w-50" for="artist">Instagram</label>
                    @error('instagram')
                        <strong style="color: red; font-size: 80%;">{{ $message }}</strong>
                    @enderror
                </span>
                <span class="col-10 col-sm-5 col-md-3">
                    <input class="gate w-100 pt-3 pb-3 @error('linkedin') is-invalid @enderror" id="linkedin" type="text" placeholder="URL Linkedin Account" name="linkedin" value="@if ($user->sosmed) {!! Request::old('linkedin', $user->sosmed->linkedin) !!}@endif"/><label class="w-50"
                        for="state">Linkedin</label>
                    @error('linkedin')
                        <strong style="color: red; font-size: 80%;">{{ $message }}</strong>
                    @enderror
                </span>
                <span class="col-10 col-sm-5 col-md-3">
                    <input class="gate w-100 pt-3 pb-3 @error('youtube') is-invalid @enderror" id="youtube" type="text" placeholder="URL Youtube Account" name="youtube" value="@if ($user->sosmed) {!! Request::old('youtube', $user->sosmed->youtube) !!}@endif"/><label class="w-50"
                        for="youtube">Youtube</label>
                    @error('youtube')
                        <strong style="color: red; font-size: 80%;">{{ $message }}</strong>
                    @enderror    
                </span>
            </div>
        </div>
        <div class="container d-flex justify-content-center">
            <a href="#" class="boton">
                Update    
            </a>
        </div>
    </form>


<br><br><br><br><br><br><br><br>







@endsection


@push('script')
    <script>
        $(window).ready(function(){
        $(".boton").wrapInner('<div class=botontext></div>');
            
            $(".botontext").clone().appendTo( $(".boton") );
            
            $(".boton").append('<span class="twist"></span><span class="twist"></span><span class="twist"></span><span class="twist"></span>');
            
            $(".twist").css("width", "25%").css("width", "+=3px");

            $('.boton').click( () => {
                $('#formEditProfile').submit();
            });

            console.log('hello world');
            
        });
    </script>
@endpush